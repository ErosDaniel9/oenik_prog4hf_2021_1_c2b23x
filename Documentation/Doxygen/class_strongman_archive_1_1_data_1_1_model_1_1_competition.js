var class_strongman_archive_1_1_data_1_1_model_1_1_competition =
[
    [ "Competition", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#afae1d05e4fd1b98578e4cb7715018932", null ],
    [ "Competition", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#ad46ec0c4275a01b605d6b5fad02394f5", null ],
    [ "Competition", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a2c09df4889d9a4cc3e8d834f52d3b062", null ],
    [ "CompetitionID", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a0ee313cd18ef7b47a76a1905729a2fb9", null ],
    [ "CompetitionName", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a3360a90c1a44b259b0826ced6de36308", null ],
    [ "Competitor", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a0ebaf9ddfb6e9a04e92fd6c41c35082d", null ],
    [ "CompetitorID", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a6008bd004c4d41125ee392ceede39c98", null ],
    [ "DateOfCompetition", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a1f70dca99aebdc1b88856ca2e0f1bf21", null ],
    [ "Federation", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a7a745f9f24e1a959863bfe3294007df3", null ],
    [ "FederationID", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a050afda30e8c99714a4cb1cfad1a0edc", null ],
    [ "Placement", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a8186254c0320db064523cd9515289391", null ],
    [ "Prize", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html#a88a6a6c972debd0b9a1f21ccfa7435a1", null ]
];