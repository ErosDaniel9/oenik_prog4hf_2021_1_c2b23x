var class_strongman_archive_1_1_repository_1_1_world_record_repository =
[
    [ "WorldRecordRepository", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html#acb619dd0950f89fa4daee1f99053b906", null ],
    [ "Create", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html#a3383321afeb00d61ec7ed6df66c897f2", null ],
    [ "Delete", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html#a2e9c662d9975e8b3467e6cbc87945c18", null ],
    [ "GetAll", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html#a315d10ea64df161ca9d22abd23bbceb1", null ],
    [ "GetById", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html#ab52fa0ebb40f28d16df1bc7e8fdf6ec0", null ],
    [ "SaveChanges", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html#a565409a2826e8e0de8a404dcb1444822", null ],
    [ "Update", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html#a41734807e8ebaff46cdea0e39c22b9df", null ]
];