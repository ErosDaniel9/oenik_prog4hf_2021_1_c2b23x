var class_strongman_archive_1_1_repository_1_1_competition_repository =
[
    [ "CompetitionRepository", "class_strongman_archive_1_1_repository_1_1_competition_repository.html#ae2851a74152f9c6f17ae02bbc2524c54", null ],
    [ "Create", "class_strongman_archive_1_1_repository_1_1_competition_repository.html#a0442afebba25da204484cb2249a85b80", null ],
    [ "Delete", "class_strongman_archive_1_1_repository_1_1_competition_repository.html#a58e14ad98e9b196849ec4b0bcc7ceb47", null ],
    [ "GetAll", "class_strongman_archive_1_1_repository_1_1_competition_repository.html#ab130f244c5d54430599b4729c7689f14", null ],
    [ "GetById", "class_strongman_archive_1_1_repository_1_1_competition_repository.html#a6228458334e5fda6d4999bec492f79fa", null ],
    [ "SaveChanges", "class_strongman_archive_1_1_repository_1_1_competition_repository.html#afe9f721759eaec114f34e602a840ea30", null ],
    [ "Update", "class_strongman_archive_1_1_repository_1_1_competition_repository.html#a6b14c0ad83c83e20c0e589d4f7eef476", null ]
];