var namespace_strongman_archive_1_1_repository_1_1_interfaces =
[
    [ "ICompetitionRepository", "interface_strongman_archive_1_1_repository_1_1_interfaces_1_1_i_competition_repository.html", null ],
    [ "ICompetitorRepository", "interface_strongman_archive_1_1_repository_1_1_interfaces_1_1_i_competitor_repository.html", null ],
    [ "IFederationRepository", "interface_strongman_archive_1_1_repository_1_1_interfaces_1_1_i_federation_repository.html", null ],
    [ "IWorldRecordRepository", "interface_strongman_archive_1_1_repository_1_1_interfaces_1_1_i_world_record_repository.html", null ]
];