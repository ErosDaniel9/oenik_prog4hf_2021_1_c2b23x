var class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic =
[
    [ "GetAll< T >", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#ae8f7404e4e4fb85600a4ce384929b72e", null ],
    [ "GetCompetitionsInAGivenYear", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#a43a00a63df35ef723904fcfa98eb55b2", null ],
    [ "GetCompetitorsWhoEarnedMoreThan", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#a3d7875e00fd631aca496bd0730ed1f57", null ],
    [ "GetCompetitorsWhoEarnedMoreThanAsync", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#ad1fa884b37098677fe10e88b5836c0c0", null ],
    [ "GetCompetitorsWithMoreWorldRecordsThan", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#ab53498f02b44904ee91e0431e2b348c7", null ],
    [ "GetCompetitorsWithMoreWorldRecordsThanAsync", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#a8f8acbaec049f562932c0676585ce19e", null ],
    [ "GetFederationsWhereAveragePrizeGivenIsBiggerThan", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#ab63ff9a06fec2723bcc444ed1b9a4e49", null ],
    [ "GetFederationsWhereAveragePrizeGivenIsBiggerThanAsync", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#a35143e56675fecb3655feb694bac9d1a", null ],
    [ "GetOne< T >", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html#aabdff8844896184ddca990f3e97f5c37", null ]
];