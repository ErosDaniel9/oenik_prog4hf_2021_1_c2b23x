var class_strongman_archive_1_1_repository_1_1_federation_repository =
[
    [ "FederationRepository", "class_strongman_archive_1_1_repository_1_1_federation_repository.html#a056dd54b162237eb822f3e39d12a20a6", null ],
    [ "Create", "class_strongman_archive_1_1_repository_1_1_federation_repository.html#a02c1e87b68b39e8472839e131ef84b75", null ],
    [ "Delete", "class_strongman_archive_1_1_repository_1_1_federation_repository.html#aa033d1ab63bcc7d77219f14f1595856b", null ],
    [ "GetAll", "class_strongman_archive_1_1_repository_1_1_federation_repository.html#aac9e953a55e29301d65007864f9e4390", null ],
    [ "GetById", "class_strongman_archive_1_1_repository_1_1_federation_repository.html#ab6752b1b2a7dfc6b8c84300c02b4624a", null ],
    [ "SaveChanges", "class_strongman_archive_1_1_repository_1_1_federation_repository.html#aff1d6dea9e863e2455142ca7b539406f", null ],
    [ "Update", "class_strongman_archive_1_1_repository_1_1_federation_repository.html#a680807a0126369e4bdbdbab426509ea6", null ]
];