var class_strongman_archive_1_1_logic_1_1_tests_1_1_strongman_specific_queries_test =
[
    [ "TestGetCompetitionsInAGivenYear", "class_strongman_archive_1_1_logic_1_1_tests_1_1_strongman_specific_queries_test.html#a832e2e6255d34f2cf963e060296f5967", null ],
    [ "TestGetCompetitorsWhoEarnedMoreThanReturnsCorrectSubset", "class_strongman_archive_1_1_logic_1_1_tests_1_1_strongman_specific_queries_test.html#ac904fd784f5103924f547159573fcd44", null ],
    [ "TestGetCompetitorsWithMoreWorldRecordsThanReturnsCorectSubset", "class_strongman_archive_1_1_logic_1_1_tests_1_1_strongman_specific_queries_test.html#a3c9b3f7c7a87d65b48cadaec2925a365", null ],
    [ "TestGetFederationsWhereAveragePrizeGivenIsBiggerThanReturnsCorrectValues", "class_strongman_archive_1_1_logic_1_1_tests_1_1_strongman_specific_queries_test.html#ac6981b02411cdaf8aa2005dc0b3f4817", null ]
];