var class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic =
[
    [ "FederationLogic", "class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic.html#a1c90470cacc90d16716316319836fd25", null ],
    [ "DeleteFrom", "class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic.html#a78747b9ee41d2a0327c6fc99e5db774d", null ],
    [ "Dispose", "class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic.html#acd11904f908977cf063caacb79a3c1ed", null ],
    [ "GetFederations", "class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic.html#a0d5310126259a7bcfc452f634dd79687", null ],
    [ "Insert", "class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic.html#a8dc2aae25b98fc276b3d5f584bd4e52f", null ],
    [ "Update", "class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic.html#a7c94445ffa2e40319fa4a043944561c3", null ]
];