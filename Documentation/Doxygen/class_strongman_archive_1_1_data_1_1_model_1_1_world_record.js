var class_strongman_archive_1_1_data_1_1_model_1_1_world_record =
[
    [ "WorldRecord", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a60564c0219502106f7cb712ae835b160", null ],
    [ "WorldRecord", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a240cbe0459c65c9ac5f1de5b723625eb", null ],
    [ "Equals", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a16af0949ad1f579d1507053ffbcc2158", null ],
    [ "GetHashCode", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a98621b1427fd61539255d389d28f8b31", null ],
    [ "ToString", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a451687fb83b7c426c1fdc377c89512ba", null ],
    [ "Competitor", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#aae296ad3ae156830a68045d0254f4187", null ],
    [ "CompetitorID", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#aaa88665053faf0a3b0eab307fb557d45", null ],
    [ "DateOfEvent", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a3a82c37a53e63f80761ced39573f9fe0", null ],
    [ "EventName", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a86466b86f5ab847b92520f4fbcb76bba", null ],
    [ "Performance", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a7bcb091570d0d8bb2efbc58d2d9c6254", null ],
    [ "TypeOfEvent", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#ad4cf8d172cbf572e14af6ac7f72266bf", null ],
    [ "Weight", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a92fd52d0a379e298d432ca9661cef224", null ],
    [ "WorldRecordID", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html#a9f274d9607585d70ff53492bb7d5680a", null ]
];