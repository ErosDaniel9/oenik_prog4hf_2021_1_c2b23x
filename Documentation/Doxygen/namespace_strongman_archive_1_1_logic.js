var namespace_strongman_archive_1_1_logic =
[
    [ "Tests", "namespace_strongman_archive_1_1_logic_1_1_tests.html", "namespace_strongman_archive_1_1_logic_1_1_tests" ],
    [ "CompetitorWithEarnedMoney", "class_strongman_archive_1_1_logic_1_1_competitor_with_earned_money.html", "class_strongman_archive_1_1_logic_1_1_competitor_with_earned_money" ],
    [ "CompetitorWithRecordNumbers", "class_strongman_archive_1_1_logic_1_1_competitor_with_record_numbers.html", "class_strongman_archive_1_1_logic_1_1_competitor_with_record_numbers" ],
    [ "FederationsWithAveragePrize", "class_strongman_archive_1_1_logic_1_1_federations_with_average_prize.html", "class_strongman_archive_1_1_logic_1_1_federations_with_average_prize" ],
    [ "IArchiveManagerLogic", "interface_strongman_archive_1_1_logic_1_1_i_archive_manager_logic.html", "interface_strongman_archive_1_1_logic_1_1_i_archive_manager_logic" ],
    [ "IBasicUserLogic", "interface_strongman_archive_1_1_logic_1_1_i_basic_user_logic.html", null ],
    [ "IGeneralQueriesLogic", "interface_strongman_archive_1_1_logic_1_1_i_general_queries_logic.html", "interface_strongman_archive_1_1_logic_1_1_i_general_queries_logic" ],
    [ "IStrongmanSpecificQueriesLogic", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic" ],
    [ "StrongmanArchiveBasicUserLogic", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic" ],
    [ "StrongmanArchiveManagerLogic", "class_strongman_archive_1_1_logic_1_1_strongman_archive_manager_logic.html", "class_strongman_archive_1_1_logic_1_1_strongman_archive_manager_logic" ]
];