var class_strongman_archive_1_1_repository_1_1_competitor_repository =
[
    [ "CompetitorRepository", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html#a376435fee29196b4d687baae398d5118", null ],
    [ "Create", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html#ac084b9e1675c7478bd9276413275f464", null ],
    [ "Delete", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html#a072ec0932e7c4b4c4ed8bbbe684674c2", null ],
    [ "GetAll", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html#a523d79139b578cff97ddfee01235767d", null ],
    [ "GetById", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html#aef04a5678107e2c398037952571ef3c6", null ],
    [ "SaveChanges", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html#a226ea0b6df9fad530cd89cfb76dba994", null ],
    [ "Update", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html#abb9fd45d2226a5c4d73104aa107afe15", null ]
];