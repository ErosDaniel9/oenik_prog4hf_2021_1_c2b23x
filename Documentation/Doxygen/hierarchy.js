var hierarchy =
[
    [ "Application", null, [
      [ "StrongmanArchive.ApiProgram.App", "class_strongman_archive_1_1_api_program_1_1_app.html", null ],
      [ "StrongmanArchive.Program.App", "class_strongman_archive_1_1_program_1_1_app.html", null ]
    ] ],
    [ "StrongmanArchive.Logic.Tests.ArchiveManagerTests", "class_strongman_archive_1_1_logic_1_1_tests_1_1_archive_manager_tests.html", null ],
    [ "Attribute", null, [
      [ "StrongmanArchive.Data.Model.HiddenAttribute", "class_strongman_archive_1_1_data_1_1_model_1_1_hidden_attribute.html", null ]
    ] ],
    [ "StrongmanArchive.Repository.Implementations.BaseRepository", "class_strongman_archive_1_1_repository_1_1_implementations_1_1_base_repository.html", [
      [ "StrongmanArchive.Repository.CompetitionRepository", "class_strongman_archive_1_1_repository_1_1_competition_repository.html", null ],
      [ "StrongmanArchive.Repository.CompetitorRepository", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html", null ],
      [ "StrongmanArchive.Repository.FederationRepository", "class_strongman_archive_1_1_repository_1_1_federation_repository.html", null ],
      [ "StrongmanArchive.Repository.WorldRecordRepository", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html", null ]
    ] ],
    [ "StrongmanArchive.Data.Model.Competition", "class_strongman_archive_1_1_data_1_1_model_1_1_competition.html", null ],
    [ "StrongmanArchive.Data.Model.Competitor", "class_strongman_archive_1_1_data_1_1_model_1_1_competitor.html", null ],
    [ "StrongmanArchive.Logic.CompetitorWithEarnedMoney", "class_strongman_archive_1_1_logic_1_1_competitor_with_earned_money.html", null ],
    [ "StrongmanArchive.Logic.CompetitorWithRecordNumbers", "class_strongman_archive_1_1_logic_1_1_competitor_with_record_numbers.html", null ],
    [ "Controller", null, [
      [ "StrongmanArchive.MVC.Controllers.FederationApiController", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_api_controller.html", null ],
      [ "StrongmanArchive.MVC.Controllers.FederationController", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_controller.html", null ],
      [ "StrongmanArchive.MVC.Controllers.HomeController", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "StrongmanArchive.Data.Model.StrongmanArchiveContext", "class_strongman_archive_1_1_data_1_1_model_1_1_strongman_archive_context.html", null ]
    ] ],
    [ "StrongmanArchive.MVC.Models.ErrorViewModel", "class_strongman_archive_1_1_m_v_c_1_1_models_1_1_error_view_model.html", null ],
    [ "StrongmanArchive.Data.Model.Federation", "class_strongman_archive_1_1_data_1_1_model_1_1_federation.html", null ],
    [ "StrongmanArchive.MVC.Models.FederationDto", "class_strongman_archive_1_1_m_v_c_1_1_models_1_1_federation_dto.html", null ],
    [ "StrongmanArchive.MVC.Extensions.FederationExtensions", "class_strongman_archive_1_1_m_v_c_1_1_extensions_1_1_federation_extensions.html", null ],
    [ "StrongmanArchive.Program.BL.FederationRepositoryFactory", "class_strongman_archive_1_1_program_1_1_b_l_1_1_federation_repository_factory.html", null ],
    [ "StrongmanArchive.Logic.FederationsWithAveragePrize", "class_strongman_archive_1_1_logic_1_1_federations_with_average_prize.html", null ],
    [ "StrongmanArchive.Logic.Tests.GeneralQueriesTest", "class_strongman_archive_1_1_logic_1_1_tests_1_1_general_queries_test.html", null ],
    [ "StrongmanArchive.Logic.IArchiveManagerLogic", "interface_strongman_archive_1_1_logic_1_1_i_archive_manager_logic.html", [
      [ "StrongmanArchive.Logic.StrongmanArchiveManagerLogic", "class_strongman_archive_1_1_logic_1_1_strongman_archive_manager_logic.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "StrongmanArchive.ApiProgram.UI.EditorWindow", "class_strongman_archive_1_1_api_program_1_1_u_i_1_1_editor_window.html", null ],
      [ "StrongmanArchive.Program.UI.EditorWindow", "class_strongman_archive_1_1_program_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "StrongmanArchive.ApiProgram.BL.FederationLogic", "class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic.html", null ]
    ] ],
    [ "StrongmanArchive.ApiProgram.BL.IEditorService", "interface_strongman_archive_1_1_api_program_1_1_b_l_1_1_i_editor_service.html", [
      [ "StrongmanArchive.ApiProgram.UI.EditorServiceViaWindow", "class_strongman_archive_1_1_api_program_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "StrongmanArchive.Program.BL.IEditorService", "interface_strongman_archive_1_1_program_1_1_b_l_1_1_i_editor_service.html", [
      [ "StrongmanArchive.Program.UI.EditorServiceViaWindow", "class_strongman_archive_1_1_program_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "IEquatable", null, [
      [ "StrongmanArchive.Common.ApiResult", "struct_strongman_archive_1_1_common_1_1_api_result.html", null ]
    ] ],
    [ "StrongmanArchive.ApiProgram.BL.IFederationLogic", "interface_strongman_archive_1_1_api_program_1_1_b_l_1_1_i_federation_logic.html", [
      [ "StrongmanArchive.ApiProgram.BL.FederationLogic", "class_strongman_archive_1_1_api_program_1_1_b_l_1_1_federation_logic.html", null ]
    ] ],
    [ "StrongmanArchive.Program.BL.IFederationLogic", "interface_strongman_archive_1_1_program_1_1_b_l_1_1_i_federation_logic.html", [
      [ "StrongmanArchive.Program.BL.FederationLogic", "class_strongman_archive_1_1_program_1_1_b_l_1_1_federation_logic.html", null ]
    ] ],
    [ "StrongmanArchive.Logic.IGeneralQueriesLogic", "interface_strongman_archive_1_1_logic_1_1_i_general_queries_logic.html", [
      [ "StrongmanArchive.Logic.IBasicUserLogic", "interface_strongman_archive_1_1_logic_1_1_i_basic_user_logic.html", [
        [ "StrongmanArchive.Logic.StrongmanArchiveBasicUserLogic", "class_strongman_archive_1_1_logic_1_1_strongman_archive_basic_user_logic.html", null ]
      ] ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "StrongmanArchive.Repository.IRepository< T >", "interface_strongman_archive_1_1_repository_1_1_i_repository.html", null ],
    [ "StrongmanArchive.Repository.IRepository< Competition >", "interface_strongman_archive_1_1_repository_1_1_i_repository.html", [
      [ "StrongmanArchive.Repository.Interfaces.ICompetitionRepository", "interface_strongman_archive_1_1_repository_1_1_interfaces_1_1_i_competition_repository.html", [
        [ "StrongmanArchive.Repository.CompetitionRepository", "class_strongman_archive_1_1_repository_1_1_competition_repository.html", null ]
      ] ]
    ] ],
    [ "StrongmanArchive.Repository.IRepository< Competitor >", "interface_strongman_archive_1_1_repository_1_1_i_repository.html", [
      [ "StrongmanArchive.Repository.Interfaces.ICompetitorRepository", "interface_strongman_archive_1_1_repository_1_1_interfaces_1_1_i_competitor_repository.html", [
        [ "StrongmanArchive.Repository.CompetitorRepository", "class_strongman_archive_1_1_repository_1_1_competitor_repository.html", null ]
      ] ]
    ] ],
    [ "StrongmanArchive.Repository.IRepository< Federation >", "interface_strongman_archive_1_1_repository_1_1_i_repository.html", [
      [ "StrongmanArchive.Repository.Interfaces.IFederationRepository", "interface_strongman_archive_1_1_repository_1_1_interfaces_1_1_i_federation_repository.html", [
        [ "StrongmanArchive.Repository.FederationRepository", "class_strongman_archive_1_1_repository_1_1_federation_repository.html", null ]
      ] ]
    ] ],
    [ "StrongmanArchive.Repository.IRepository< WorldRecord >", "interface_strongman_archive_1_1_repository_1_1_i_repository.html", [
      [ "StrongmanArchive.Repository.Interfaces.IWorldRecordRepository", "interface_strongman_archive_1_1_repository_1_1_interfaces_1_1_i_world_record_repository.html", [
        [ "StrongmanArchive.Repository.WorldRecordRepository", "class_strongman_archive_1_1_repository_1_1_world_record_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "StrongmanArchive.ApiProgram.MyIoC", "class_strongman_archive_1_1_api_program_1_1_my_io_c.html", null ],
      [ "StrongmanArchive.Program.MyIoC", "class_strongman_archive_1_1_program_1_1_my_io_c.html", null ]
    ] ],
    [ "StrongmanArchive.Logic.IStrongmanSpecificQueriesLogic", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html", [
      [ "StrongmanArchive.Logic.IBasicUserLogic", "interface_strongman_archive_1_1_logic_1_1_i_basic_user_logic.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "StrongmanArchive.ApiProgram.UI.EstablishmentDateToStringConverter", "class_strongman_archive_1_1_api_program_1_1_u_i_1_1_establishment_date_to_string_converter.html", null ],
      [ "StrongmanArchive.ApiProgram.UI.StatusToBrushConverter", "class_strongman_archive_1_1_api_program_1_1_u_i_1_1_status_to_brush_converter.html", null ],
      [ "StrongmanArchive.Program.UI.EstablishmentDateToStringConverter", "class_strongman_archive_1_1_program_1_1_u_i_1_1_establishment_date_to_string_converter.html", null ],
      [ "StrongmanArchive.Program.UI.StatusToBrushConverter", "class_strongman_archive_1_1_program_1_1_u_i_1_1_status_to_brush_converter.html", null ]
    ] ],
    [ "ObservableObject", null, [
      [ "StrongmanArchive.ApiProgram.Data.Federation", "class_strongman_archive_1_1_api_program_1_1_data_1_1_federation.html", null ],
      [ "StrongmanArchive.Program.Data.Federation", "class_strongman_archive_1_1_program_1_1_data_1_1_federation.html", null ]
    ] ],
    [ "StrongmanArchive.MVC.Program", "class_strongman_archive_1_1_m_v_c_1_1_program.html", null ],
    [ "RazorPage", null, [
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views_Federation_FederationUpdate", "class_asp_net_core_1_1_views___federation___federation_update.html", null ],
      [ "AspNetCore.Views_Federation_Warning", "class_asp_net_core_1_1_views___federation___warning.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ]
    ] ],
    [ "RazorPage< IEnumerable< FederationDto >>", null, [
      [ "AspNetCore.Views_Federation_Index", "class_asp_net_core_1_1_views___federation___index.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "StrongmanArchive.ApiProgram.MyIoC", "class_strongman_archive_1_1_api_program_1_1_my_io_c.html", null ],
      [ "StrongmanArchive.Program.MyIoC", "class_strongman_archive_1_1_program_1_1_my_io_c.html", null ]
    ] ],
    [ "StrongmanArchive.MVC.Startup", "class_strongman_archive_1_1_m_v_c_1_1_startup.html", null ],
    [ "StrongmanArchive.Logic.Tests.StrongmanSpecificQueriesTest", "class_strongman_archive_1_1_logic_1_1_tests_1_1_strongman_specific_queries_test.html", null ],
    [ "ViewModelBase", null, [
      [ "StrongmanArchive.ApiProgram.VM.EditorViewModel", "class_strongman_archive_1_1_api_program_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "StrongmanArchive.ApiProgram.VM.MainViewModel", "class_strongman_archive_1_1_api_program_1_1_v_m_1_1_main_view_model.html", null ],
      [ "StrongmanArchive.Program.VM.EditorViewModel", "class_strongman_archive_1_1_program_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "StrongmanArchive.Program.VM.MainViewModel", "class_strongman_archive_1_1_program_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "StrongmanArchive.ApiProgram.MainWindow", "class_strongman_archive_1_1_api_program_1_1_main_window.html", null ],
      [ "StrongmanArchive.ApiProgram.UI.EditorWindow", "class_strongman_archive_1_1_api_program_1_1_u_i_1_1_editor_window.html", null ],
      [ "StrongmanArchive.Program.MainWindow", "class_strongman_archive_1_1_program_1_1_main_window.html", null ],
      [ "StrongmanArchive.Program.UI.EditorWindow", "class_strongman_archive_1_1_program_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "StrongmanArchive.Data.Model.WorldRecord", "class_strongman_archive_1_1_data_1_1_model_1_1_world_record.html", null ]
];