var indexSectionsWithContent =
{
  0: "abcdefghimnoprstuvwx",
  1: "abcefghimpsvw",
  2: "asx",
  3: "acdefghimoprstuw",
  4: "cfs",
  5: "e",
  6: "drtw",
  7: "acdefhimnprstuvw",
  8: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Pages"
};

