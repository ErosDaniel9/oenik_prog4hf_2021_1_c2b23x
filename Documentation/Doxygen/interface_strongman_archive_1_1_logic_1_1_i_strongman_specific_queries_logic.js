var interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic =
[
    [ "GetCompetitionsInAGivenYear", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html#a5df8810b0059c4f6146c81cb1a33b991", null ],
    [ "GetCompetitorsWhoEarnedMoreThan", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html#aa83eb74b509391195dfc16c736c8015a", null ],
    [ "GetCompetitorsWhoEarnedMoreThanAsync", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html#a87835483e6686224768383696d641763", null ],
    [ "GetCompetitorsWithMoreWorldRecordsThan", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html#a3e685ce205c136078beba04d96eef512", null ],
    [ "GetCompetitorsWithMoreWorldRecordsThanAsync", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html#aa03caf2616bfe9757a119c597906f791", null ],
    [ "GetFederationsWhereAveragePrizeGivenIsBiggerThan", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html#a439f386fb0e03212a6ec59d4a5efe4ca", null ],
    [ "GetFederationsWhereAveragePrizeGivenIsBiggerThanAsync", "interface_strongman_archive_1_1_logic_1_1_i_strongman_specific_queries_logic.html#a4220e5bd22b2ee68b753ec5341690b10", null ]
];