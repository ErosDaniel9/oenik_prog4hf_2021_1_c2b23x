var interface_strongman_archive_1_1_repository_1_1_i_repository =
[
    [ "Create", "interface_strongman_archive_1_1_repository_1_1_i_repository.html#a8fe1bfd01814b1bc90b7bd983f008556", null ],
    [ "Delete", "interface_strongman_archive_1_1_repository_1_1_i_repository.html#ab713f1ec935d3033152dc70d72c1b70d", null ],
    [ "GetAll", "interface_strongman_archive_1_1_repository_1_1_i_repository.html#af5324e86bdf3891d1870ced953bc8cc4", null ],
    [ "GetById", "interface_strongman_archive_1_1_repository_1_1_i_repository.html#a39c1b5c9244a647dcf2e88adb5fddb80", null ],
    [ "SaveChanges", "interface_strongman_archive_1_1_repository_1_1_i_repository.html#a604b203f9c9b018201eef8655b947f22", null ],
    [ "Update", "interface_strongman_archive_1_1_repository_1_1_i_repository.html#af028955154cc6acb7ee624c0526f0828", null ]
];