var class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_api_controller =
[
    [ "FederationApiController", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_api_controller.html#a4801e2606589e43bdc18bc5ce03c974c", null ],
    [ "AddFederation", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_api_controller.html#a68a39e33889f021da7d801900d2c7e37", null ],
    [ "DeleteFederation", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_api_controller.html#a0d28e3d46a501b8a8071d0e803bbdfc0", null ],
    [ "GetAllFederations", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_api_controller.html#ab357d16a4df3399494c24ce114c64cfd", null ],
    [ "GetFederation", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_api_controller.html#a435177e2bf210524deb632ac5689c094", null ],
    [ "UpdateFederation", "class_strongman_archive_1_1_m_v_c_1_1_controllers_1_1_federation_api_controller.html#a3e32eeceaf7c9460c3041b5ec9fdcb42", null ]
];