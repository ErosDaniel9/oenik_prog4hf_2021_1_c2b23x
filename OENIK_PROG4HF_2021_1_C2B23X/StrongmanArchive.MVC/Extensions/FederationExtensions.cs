﻿// <copyright file="FederationExtensions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.MVC.Extensions
{
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.MVC.Models;

    /// <summary>
    /// A class containing federation transformation extension methods.
    /// </summary>
    public static class FederationExtensions
    {
        /// <summary>
        /// Converts a db object to a dto.
        /// </summary>
        /// <param name="federation">The db object.</param>
        /// <returns>A dto with the correct values.</returns>
        public static FederationDto ToDto(this Federation federation)
        {
            return new FederationDto
            {
                Name = federation?.FederationName,
                Id = federation.FederationID,
                Establisher = federation.EstablisherName,
                EstablishedIn = federation.EstablishmentDate,
                NetWorth = federation.NetWorth,
                IsActive = federation.Active,
            };
        }

        /// <summary>
        /// Returns a db object without id(!).
        /// </summary>
        /// <param name="federationDto">The dto containing our data.</param>
        /// <returns>A db object without id.</returns>
        public static Federation ToDbObj(this FederationDto federationDto)
        {
            return new Federation
            {
                FederationName = federationDto?.Name,
                EstablisherName = federationDto.Establisher,
                EstablishmentDate = federationDto.EstablishedIn,
                Active = federationDto.IsActive,
                NetWorth = federationDto.NetWorth,
            };
        }

        /// <summary>
        /// Returns a db object with id(!).
        /// </summary>
        /// <param name="federationDto">The dto containing our data.</param>
        /// <returns>A db object with id.</returns>
        public static Federation ToDbObjWithId(this FederationDto federationDto)
        {
            return new Federation
            {
                FederationName = federationDto?.Name,
                FederationID = federationDto.Id,
                EstablisherName = federationDto.Establisher,
                EstablishmentDate = federationDto.EstablishedIn,
                Active = federationDto.IsActive,
                NetWorth = federationDto.NetWorth,
            };
        }
    }
}
