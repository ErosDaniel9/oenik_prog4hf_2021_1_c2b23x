﻿// <copyright file="FederationController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Logic;
    using StrongmanArchive.MVC.Models;
    using StrongmanArchive.Repository;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// The controller working on our federation models.
    /// </summary>
    public class FederationController : Controller
    {
        private List<FederationDto> federations;
        private IBasicUserLogic dqlLogic;
        private IArchiveManagerLogic dmlLogic;
        private IFederationRepository federationRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FederationController"/> class.
        /// </summary>
        /// <param name="dqlLogic">The logic executing our dql commands.</param>
        /// <param name="dmlLogic">The logic executing our dml commands.</param>
        /// <param name="federationRepository">The repository for our federations.</param>
        public FederationController(IBasicUserLogic dqlLogic, IArchiveManagerLogic dmlLogic, IFederationRepository federationRepository)
        {
            this.dqlLogic = dqlLogic;
            this.dmlLogic = dmlLogic;
            this.federationRepository = federationRepository;
            this.federations = new List<FederationDto>();
        }

        /// <summary>
        /// Return the home page.
        /// </summary>
        /// <returns>The home page.</returns>
        public IActionResult Index()
        {
            this.PumpDataIntoFederations();

            return this.View("Index", this.federations);
        }

        /// <summary>
        /// Inserts an element to our federation list, then return us to our home page.
        /// </summary>
        /// <param name="federation">The element to insert.</param>
        /// <returns>The home page.</returns>
        public IActionResult Insert(FederationDto federation)
        {
            if (this.CheckIfValuesAreValid(federation))
            {
                this.AddFederation(federation);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Deletes the item with the given id from our federation list, then returns us to our home page.
        /// </summary>
        /// <param name="id">The primary key of the element we want to delete.</param>
        /// <returns>The home page.</returns>
        public IActionResult Delete(int id)
        {
            this.RemoveFederation(id);
            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Gets the data of the federation with the given id, then redirects us to our update window.
        /// </summary>
        /// <param name="id">The primary key of the federation we want to display.</param>
        /// <returns>The update view.</returns>
        public IActionResult RedirectToUpdateWindow(int id)
        {
            Models.FederationDto displayElement = new Models.FederationDto();
            displayElement.TransformDbElementIntoFederation(this.dqlLogic.GetOne(this.federationRepository, id));

            return this.View("FederationUpdate", displayElement);
        }

        /// <summary>
        /// Updates the given federation, then redirects us to our home page.
        /// </summary>
        /// <param name="federation">The federation containing our new values.</param>
        /// <returns>The home page.</returns>
        public IActionResult Update(Models.FederationDto federation)
        {
            if (this.CheckIfValuesAreValid(federation))
            {
                this.UpdateFederation(federation);
                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                return this.View("FederationUpdate", federation);
            }
        }

        private bool CheckIfValuesAreValid(Models.FederationDto federation)
        {
            if (federation == null)
            {
                this.TempData["warning"] = new ArgumentNullException(nameof(federation));
                throw new ArgumentNullException(nameof(federation));
            }

            if (federation.Name.Length < 5 ||
                federation.Name.Length > 100 ||
                federation.Establisher.Length < 5 ||
                federation.Establisher.Length > 100)
            {
                this.TempData["warning"] = "Federation name and establisher length should fall between 5 and 100 characters.";
                return false;
            }
            else if (federation.NetWorth < -200000 || federation.NetWorth > 200000)
            {
                this.TempData["warning"] = "Federation net worth should be between -200000 and 200000";
                return false;
            }
            else
            {
                return true;
            }
        }

        private void UpdateFederation(Models.FederationDto federation)
        {
            Data.Model.Federation elementToUpdate = new Data.Model.Federation(federation.Name, federation.EstablishedIn, federation.Establisher, federation.IsActive, federation.NetWorth);
            elementToUpdate.FederationID = federation.Id;
            this.dmlLogic.Update(this.federationRepository, elementToUpdate);
        }

        private void AddFederation(Models.FederationDto federation)
        {
            Data.Model.Federation elementToInsert = new Data.Model.Federation(federation.Name, federation.EstablishedIn, federation.Establisher, federation.IsActive, federation.NetWorth);
            this.dmlLogic.InsertInto(this.federationRepository, elementToInsert);
        }

        private void RemoveFederation(int federationId)
        {
            Data.Model.Federation elementToDelete = this.dqlLogic.GetOne(this.federationRepository, federationId);
            if (elementToDelete != null)
            {
                this.dmlLogic.DeleteFrom(this.federationRepository, elementToDelete);
            }
        }

        private void PumpDataIntoFederations()
        {
            this.dqlLogic
                .GetAll<Federation>(this.federationRepository)
                .ToList()
                .ForEach(dataFed =>
                {
                    Models.FederationDto mvcFed = new Models.FederationDto();
                    mvcFed.TransformDbElementIntoFederation(dataFed);
                    this.federations.Add(mvcFed);
                });
        }
    }
}
