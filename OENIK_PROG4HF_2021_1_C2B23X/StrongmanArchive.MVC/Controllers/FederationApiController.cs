﻿// <copyright file="FederationApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using StrongmanArchive.Common;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Logic;
    using StrongmanArchive.MVC.Extensions;
    using StrongmanArchive.MVC.Models;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// An API controller implementing CRUD methods for the federation table.
    /// </summary>
    public class FederationApiController : Controller
    {
        private IFederationRepository federationRepository;
        private IBasicUserLogic dqlLogic;
        private IArchiveManagerLogic dmlLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="FederationApiController"/> class.
        /// </summary>
        /// <param name="federationRepository">The repository we're working on.</param>
        /// <param name="dqlLogic">The logic responsible for the dql queries.</param>
        /// <param name="dmlLogic">The logic responsible for the dml queries.</param>
        public FederationApiController(IFederationRepository federationRepository, IBasicUserLogic dqlLogic, IArchiveManagerLogic dmlLogic)
        {
            this.federationRepository = federationRepository;
            this.dqlLogic = dqlLogic;
            this.dmlLogic = dmlLogic;
        }

        /// <summary>
        /// Returns an enumerable collection containing the federations.
        /// </summary>
        /// <returns>An enumerable collection containing the federations.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<FederationDto> GetAllFederations()
        {
            List<FederationDto> federations = new List<FederationDto>();
            this.dqlLogic.GetAll(this.federationRepository)
                .ToList()
                .ForEach(fed => federations.Add(fed.ToDto()));
            return federations;
        }

        /// <summary>
        /// Returns the federation with the given primary key.
        /// </summary>
        /// <param name="id">The primary key.</param>
        /// <returns>The federation with the given primary key.</returns>
        [HttpGet]
        [ActionName("one")]
        public FederationDto GetFederation(int id) => this.dqlLogic.GetOne(this.federationRepository, id).ToDto();

        /// <summary>
        /// Inserts a single federation to the database, then returns an <see cref="ApiResult"/> containing the Success value, and either the ID of the inserted item, or the error message.
        /// </summary>
        /// <param name="inputFederation">The federation we want to insert.</param>
        /// <returns>An <see cref="ApiResult"/> containing either the ID of the inserted element, or the error message. Also containis the Success value.</returns>
        [HttpPost]
        [ActionName("insert")]
        public ApiResult AddFederation([FromBody]FederationDto inputFederation)
        {
            try
            {
                Federation convertedFederation = inputFederation.ToDbObj();
                this.dmlLogic.InsertInto(this.federationRepository, convertedFederation);
                return new ApiResult { Success = true, Id = convertedFederation.FederationID };
            }
            catch (ArgumentNullException ex)
            {
                return new ApiResult { ErrorMsg = ex.Message, Success = false };
            }
        }

        /// <summary>
        /// Updates the given federation, then returns an <see cref="ApiResult"/> containing the Success value, and either the ID of the updated item, or the error message.
        /// </summary>
        /// <param name="inputFederation">The federation with the new values.</param>
        /// <returns>An <see cref="ApiResult"/> containing the Success value, and either the ID of the inserted item, or the error message.</returns>
        [HttpPost]
        [ActionName("update")]
        public ApiResult UpdateFederation([FromBody]FederationDto inputFederation)
        {
            try
            {
                Federation convertedFederation = inputFederation.ToDbObjWithId();
                this.dmlLogic.Update(this.federationRepository, convertedFederation);
                return new ApiResult { Success = true, Id = convertedFederation.FederationID };
            }
            catch (ArgumentNullException ex)
            {
                return new ApiResult { ErrorMsg = ex.Message, Success = false };
            }
        }

        /// <summary>
        /// Deletes the given federation, then returns an <see cref="ApiResult"/> containing the Success value, and either the ID of the deleted item, or the error message.
        /// </summary>
        /// <param name="inputFederation">The federation with the delete data.</param>
        /// <returns>An <see cref="ApiResult"/> containing the Success value, and either the ID of the inserted item, or the error message.</returns>
        [HttpPost]
        [ActionName("delete")]
        public ApiResult DeleteFederation([FromBody] FederationDto inputFederation)
        {
            try
            {
                Federation convertedFederation = inputFederation.ToDbObjWithId();
                this.dmlLogic.DeleteFrom(this.federationRepository, convertedFederation);
                return new ApiResult { Success = true, Id = convertedFederation.FederationID };
            }
            catch (ArgumentNullException ex)
            {
               return new ApiResult { ErrorMsg = ex.Message, Success = false };
            }
        }
    }
}
