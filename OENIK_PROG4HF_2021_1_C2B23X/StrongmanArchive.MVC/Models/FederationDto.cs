﻿// <copyright file="FederationDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.MVC.Models
{
    using System;
    using System.Linq;

    /// <summary>
    /// Federation class for data binding.
    /// </summary>
    public class FederationDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FederationDto"/> class.
        /// </summary>
        public FederationDto()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FederationDto"/> class.
        /// </summary>
        /// <param name="id">The primary key of the federation.</param>
        /// <param name="name">The name of the federation.</param>
        /// <param name="establishedIn">The date the federation was established in.</param>
        /// <param name="establisher">The name of the federations' establisher.</param>
        /// <param name="isActive">A value indicating whether the federation is still active.</param>
        /// <param name="netWorth">The net worth of the federation.</param>
        public FederationDto(int id, string name, DateTime establishedIn, string establisher, bool isActive, int netWorth)
        {
            this.Id = id;
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.EstablishedIn = establishedIn;
            this.Establisher = establisher ?? throw new ArgumentNullException(nameof(establisher));
            this.IsActive = isActive;
            this.NetWorth = netWorth;
        }

        /// <summary>
        /// Gets or sets the Identifier of the federation.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name of the federation.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the date the federation was established.
        /// </summary>
        public DateTime EstablishedIn { get; set; }

        /// <summary>
        /// Gets or sets the name of the establisher.
        /// </summary>
        public string Establisher { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the federation is still active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the net worth of the federation.
        /// </summary>
        public int NetWorth { get; set; }

        /// <summary>
        /// Copies all values from another object.
        /// </summary>
        /// <param name="other">The source of the copy.</param>
        public void CopyFrom(FederationDto other)
        {
            this.GetType().GetProperties().ToList().ForEach(p => p.SetValue(this, p.GetValue(other)));
        }

        /// <summary>
        /// Transforms a Federation Db object into a UI-level federation object.
        /// </summary>
        /// <param name="dbObj">The object we want to transform.</param>
        public void TransformDbElementIntoFederation(StrongmanArchive.Data.Model.Federation dbObj)
        {
            if (dbObj == null)
            {
                string errorMsg = "The given object is empty.";
                throw new ArgumentNullException(errorMsg);
            }

            this.Id = dbObj.FederationID;
            this.Name = dbObj.FederationName;
            this.EstablishedIn = dbObj.EstablishmentDate;
            this.Establisher = dbObj.EstablisherName;
            this.IsActive = dbObj.Active;
            this.NetWorth = dbObj.NetWorth;
        }
    }
}
