﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;

[assembly: CLSCompliant(false)]

namespace StrongmanArchive.Common
{
    /// <summary>
    /// The struct containing basic Api result data.
    /// </summary>
    public struct ApiResult : IEquatable<ApiResult>
    {
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string ErrorMsg { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the element.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether our request was successful.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Returns a value indicating whether the left and right <see cref="ApiResult"/> classes are equal.
        /// </summary>
        /// <param name="left">The left <see cref="ApiResult"/> class.</param>
        /// <param name="right">The right <see cref="ApiResult"/> class.</param>
        /// <returns>A value indicating whether the left and right <see cref="ApiResult"/> classes are equal.</returns>
        public static bool operator ==(ApiResult left, ApiResult right) => left.Equals(right);

        /// <summary>
        /// Returns a value indicating whether the left and right <see cref="ApiResult"/> classes are not equal.
        /// </summary>
        /// <param name="left">The left <see cref="ApiResult"/> class.</param>
        /// <param name="right">The right <see cref="ApiResult"/> class.</param>
        /// <returns>A value indicating whether the left and right <see cref="ApiResult"/> classes are not equal.</returns>
        public static bool operator !=(ApiResult left, ApiResult right) => !left.Equals(right);

        /// <inheritdoc/>
        public override bool Equals(object obj) => base.Equals(obj);

        /// <inheritdoc/>
        public override int GetHashCode() => base.GetHashCode();

        /// <inheritdoc/>
        public bool Equals(ApiResult other) => this == other;
    }
}