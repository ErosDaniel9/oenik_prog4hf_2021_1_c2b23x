﻿// <copyright file="StrongmanArchiveContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Data.Model
{
    using System;
    using System.Globalization;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// A DbContext class for the StrongmanArchive database.
    /// </summary>
    public class StrongmanArchiveContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StrongmanArchiveContext"/> class.
        /// </summary>
        public StrongmanArchiveContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets a DbSet containing the competition entities.
        /// </summary>
        public DbSet<Competition> Competitions { get; set; }

        /// <summary>
        /// Gets or sets a DbSet containing  the federations.
        /// </summary>
        public DbSet<Federation> Federations { get; set; }

        /// <summary>
        /// Gets or sets a collection containing the competitors.
        /// </summary>
        public DbSet<Competitor> Competitors { get; set; }

        /// <summary>
        /// Gets or sets a collectiion containing the world records.
        /// </summary>
        public DbSet<WorldRecord> WorldRecords { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\StrongmanArchive.mdf;Integrated Security=True");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Competitor cor1 = new Competitor("Sanyi", System.DateTime.Parse("1967.03.11", CultureInfo.InvariantCulture), "Sesame street 3.", "+36 30 123 5334", 190, 200) { CompetitorID = 1 };
            Competitor cor2 = new Competitor("Eddie Hall", System.DateTime.Parse("1970.02.05", CultureInfo.InvariantCulture), "Deadlift street 22.", "+36 30 456 7721", 190, 200) { CompetitorID = 2 };
            Competitor cor3 = new Competitor("Brian Shaw", System.DateTime.Parse("1968.09.01", CultureInfo.InvariantCulture), "4time street 2.", "+36 72 864 1234", 190, 200) { CompetitorID = 3 };
            Competitor cor4 = new Competitor("Jon Paul Sigmarsson", System.DateTime.Parse("1960.05.21", CultureInfo.InvariantCulture), "Viking street 11.", "+36 12 454 3412", 190, 200) { CompetitorID = 4 };
            Competitor cor5 = new Competitor("Big Z", System.DateTime.Parse("1967.11.07", CultureInfo.InvariantCulture), "GOAT street 5.", "+36 22 243 1245", 190, 200) { CompetitorID = 5 };

            //------------------------------------------------------------------------------------------------------------------------------
            Federation f1 = new Federation("Arnold Federation", System.DateTime.Parse("1920.08.20", CultureInfo.InvariantCulture), "Arnold Schwarzenegger", true, 100000) { FederationID = 1 };
            Federation f2 = new Federation("World's Strongman Federation", System.DateTime.Parse("1900.02.02", CultureInfo.InvariantCulture), "Nobody", true, 150000) { FederationID = 2 };
            Federation f3 = new Federation("Highlander Foundation", System.DateTime.Parse("1912.07.12", CultureInfo.InvariantCulture), "Doesn't Matter", false, -20000) { FederationID = 3 };
            Federation f4 = new Federation("World's Natural Strongman Foundation", System.DateTime.Parse("1920.08.20", CultureInfo.InvariantCulture), "Lars Ulrich", false, -500) { FederationID = 4 };

            //------------------------------------------------------------------------------------------------------------------------------
            Competition con1 = new Competition() { CompetitionID = 1, CompetitionName = "Arnold Classic 2017", DateOfCompetition = System.DateTime.Parse("2017.03.11", CultureInfo.InvariantCulture), Placement = 1, Prize = 10000, FederationID = 1, CompetitorID = 1 };
            Competition con2 = new Competition() { CompetitionID = 2, CompetitionName = "Arnold Classic 2017", DateOfCompetition = System.DateTime.Parse("2017.03.11", CultureInfo.InvariantCulture), Placement = 2, Prize = 7000, FederationID = 1, CompetitorID = 3 };
            Competition con3 = new Competition() { CompetitionID = 3, CompetitionName = "Arnold Classic 2017", DateOfCompetition = System.DateTime.Parse("2017.03.11", CultureInfo.InvariantCulture), Placement = 3, Prize = 5000, FederationID = 1, CompetitorID = 4 };
            Competition con4 = new Competition() { CompetitionID = 4, CompetitionName = "Arnold Classic 2018", DateOfCompetition = System.DateTime.Parse("2018.03.11", CultureInfo.InvariantCulture), Placement = 1, Prize = 10000, FederationID = 1, CompetitorID = 4 };
            Competition con5 = new Competition() { CompetitionID = 5, CompetitionName = "Arnold Classic 2018", DateOfCompetition = System.DateTime.Parse("2018.03.11", CultureInfo.InvariantCulture), Placement = 2, Prize = 7000, FederationID = 1, CompetitorID = 2 };
            Competition con6 = new Competition() { CompetitionID = 6, CompetitionName = "Arnold Classic 2018", DateOfCompetition = System.DateTime.Parse("2018.03.11", CultureInfo.InvariantCulture), Placement = 3, Prize = 5000, FederationID = 1, CompetitorID = 3 };
            Competition con7 = new Competition() { CompetitionID = 7, CompetitionName = "World's Strongest Man 2018", DateOfCompetition = System.DateTime.Parse("2018.08.05", CultureInfo.InvariantCulture), Placement = 1, Prize = 20000, FederationID = 2, CompetitorID = 1 };
            Competition con8 = new Competition() { CompetitionID = 8, CompetitionName = "World's Strongest Man 2018", DateOfCompetition = System.DateTime.Parse("2018.08.05", CultureInfo.InvariantCulture), Placement = 2, Prize = 15000, FederationID = 2, CompetitorID = 2 };
            Competition con9 = new Competition() { CompetitionID = 9, CompetitionName = "World's Strongest Man 2018", DateOfCompetition = System.DateTime.Parse("2018.08.05", CultureInfo.InvariantCulture), Placement = 3, Prize = 10000, FederationID = 2, CompetitorID = 3 };

            //------------------------------------------------------------------------------------------------------------------------------
            WorldRecord w1 = new WorldRecord() { WorldRecordID = 1, DateOfEvent = System.DateTime.Parse("2000.05.24", CultureInfo.InvariantCulture), TypeOfEvent = EventType.Weight, EventName = "Deadlift", Weight = 453, Performance = 1, CompetitorID = 3 };
            WorldRecord w2 = new WorldRecord() { WorldRecordID = 2, DateOfEvent = System.DateTime.Parse("2007.03.24", CultureInfo.InvariantCulture), TypeOfEvent = EventType.Distance, EventName = "Yoke Walk", Weight = 600, Performance = 15, CompetitorID = 2 };
            WorldRecord w3 = new WorldRecord() { WorldRecordID = 3, DateOfEvent = System.DateTime.Parse("1970.07.04", CultureInfo.InvariantCulture), TypeOfEvent = EventType.Time, EventName = "Hercules Hold", Weight = 500, Performance = 72, CompetitorID = 4 };
            WorldRecord w4 = new WorldRecord() { WorldRecordID = 4, DateOfEvent = System.DateTime.Parse("2017.03.11", CultureInfo.InvariantCulture), TypeOfEvent = EventType.Weight, EventName = "Keg Throw", Weight = 50, Performance = 53, CompetitorID = 2 };
            WorldRecord w5 = new WorldRecord() { WorldRecordID = 5, DateOfEvent = System.DateTime.Parse("2010.09.11", CultureInfo.InvariantCulture), TypeOfEvent = EventType.Repetitions, EventName = "Deadlift", Weight = 400, Performance = 6, CompetitorID = 1 };

            //------------------------------------------------------------------------------------------------------------------------------
            if (modelBuilder != null)
            {
                modelBuilder.Entity<Competition>()
                .HasOne(c => c.Federation)
                .WithMany(f => f.Competitions)
                .HasForeignKey(c => c.FederationID)
                .OnDelete(DeleteBehavior.SetNull);

                modelBuilder.Entity<Competition>()
                    .HasOne(con => con.Competitor)
                    .WithMany(cor => cor.Competitions)
                    .HasForeignKey(con => con.CompetitorID)
                    .OnDelete(DeleteBehavior.SetNull);

                modelBuilder.Entity<WorldRecord>()
                    .HasOne(w => w.Competitor)
                    .WithMany(c => c.WorldRecords)
                    .HasForeignKey(w => w.CompetitorID)
                    .OnDelete(DeleteBehavior.Cascade);

                modelBuilder.Entity<Federation>().HasData(f1, f2, f3, f4);
                modelBuilder.Entity<Competitor>().HasData(cor1, cor2, cor3, cor4, cor5);
                modelBuilder.Entity<WorldRecord>().HasData(w1, w2, w3, w4, w5);
                modelBuilder.Entity<Competition>().HasData(con1, con2, con3, con4, con5, con6, con7, con8, con9);
            }
            else
            {
                throw new System.ArgumentNullException(nameof(modelBuilder));
            }
        }
    }
}
