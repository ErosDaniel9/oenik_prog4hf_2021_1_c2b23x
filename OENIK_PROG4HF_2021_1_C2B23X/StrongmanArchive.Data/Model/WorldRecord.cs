﻿// <copyright file="WorldRecord.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    /// <summary>
    /// An entity for the WorldRecords table.
    /// </summary>
    [Table("WorldRecords")]
    public class WorldRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorldRecord"/> class.
        /// </summary>
        /// <param name="values">The values of the class in a string list.</param>
        public WorldRecord(Collection<string> values)
        {
            if (values == null)
            {
                string errorMsg = "Üres listát adott meg.";
                throw new ArgumentNullException(errorMsg);
            }

            this.EventName = values[0];
            this.Weight = int.Parse(values[1], CultureInfo.InvariantCulture);
            this.TypeOfEvent = GetEventType(values[2]);
            this.Performance = int.Parse(values[3], CultureInfo.InvariantCulture);
            this.DateOfEvent = DateTime.Parse(values[4], CultureInfo.InvariantCulture);
            this.CompetitorID = int.Parse(values[5], CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorldRecord"/> class.
        /// </summary>
        public WorldRecord()
        {
        }

        /// <summary>
        /// Gets or sets the primary key of the entity.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorldRecordID { get; set; }

        /// <summary>
        /// Gets or sets the name of the event.
        /// </summary>
        [MaxLength(100)]
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the weight lifted by the competitor.
        /// </summary>
        [Range(50, 600)]
        public int Weight { get; set; }

        /// <summary>
        /// Gets or sets the type of the event.
        /// </summary>
        public EventType TypeOfEvent { get; set; }

        /// <summary>
        /// Gets or sets the performance of the competitor.
        /// </summary>
        public int Performance { get; set; }

        /// <summary>
        /// Gets or sets the date of the event.
        /// </summary>
        public DateTime DateOfEvent { get; set; }

        /// <summary>
        /// Gets or sets the primary key of the competitor.
        /// </summary>
        public int? CompetitorID { get; set; }

        /// <summary>
        /// Gets the competitor.
        /// </summary>
        [Hidden]
        public Competitor Competitor { get; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is WorldRecord)
            {
                WorldRecord other = obj as WorldRecord;
                return this.WorldRecordID == other.WorldRecordID &&
                     this.TypeOfEvent == other.TypeOfEvent &&
                     this.Performance == other.Performance &&
                     this.Weight == other.Weight &&
                     this.CompetitorID == other.CompetitorID;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return base.ToString();
        }

        private static EventType GetEventType(string src)
        {
            return src.ToUpperInvariant() switch
            {
                "DISTANCE" => EventType.Distance,
                "TIME" => EventType.Time,
                "REPETITIONS" => EventType.Repetitions,
                "WEIGHT" => EventType.Weight,
                _ => throw new ArrayTypeMismatchException("Given event type is not valid"),
            };
        }
    }
}
