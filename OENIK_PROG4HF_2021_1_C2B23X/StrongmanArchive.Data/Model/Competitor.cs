﻿// <copyright file="Competitor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    /// <summary>
    /// Entity for the competitor table.
    /// </summary>
    [Table("Competitors")]
    public class Competitor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Competitor"/> class.
        /// </summary>
        /// <param name="competitorName">The name of the competitor.</param>
        /// <param name="dateOfBirth">The birth date of the competitor.</param>
        /// <param name="address">The address of the competitor.</param>
        /// <param name="telephoneNumber">The telephone number of the competitor.</param>
        /// <param name="height">The height of the competitor.</param>
        /// <param name="weight">The weight of the competitor.</param>
        public Competitor(string competitorName, DateTime dateOfBirth, string address, string telephoneNumber, int height, int weight)
        {
            this.CompetitorName = competitorName;
            this.DateOfBirth = dateOfBirth;
            this.Address = address;
            this.TelephoneNumber = telephoneNumber;
            this.Height = height;
            this.Weight = weight;
            this.Competitions = new HashSet<Competition>();
            this.WorldRecords = new HashSet<WorldRecord>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Competitor"/> class.
        /// </summary>
        /// <param name="values">The values of the class in a string list.</param>
        public Competitor(Collection<string> values)
        {
            if (values == null)
            {
                string errorMsg = "Üres listát adott meg.";
                throw new ArgumentNullException(errorMsg);
            }

            this.CompetitorName = values[0];
            this.DateOfBirth = DateTime.Parse(values[1], CultureInfo.InvariantCulture);
            this.Address = values[2];
            this.TelephoneNumber = values[3];
            this.Height = int.Parse(values[4], CultureInfo.InvariantCulture);
            this.Weight = int.Parse(values[5], CultureInfo.InvariantCulture);
            this.Competitions = new HashSet<Competition>();
            this.WorldRecords = new HashSet<WorldRecord>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Competitor"/> class.
        /// </summary>
        public Competitor()
        {
            this.Competitions = new HashSet<Competition>();
            this.WorldRecords = new HashSet<WorldRecord>();
        }

        /// <summary>
        /// Gets or sets the primary key of the Entity.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompetitorID { get; set; }

        /// <summary>
        /// Gets or sets the name of the competitor.
        /// </summary>
        [MaxLength(100)]
        public string CompetitorName { get; set; }

        /// <summary>
        /// Gets or sets the birth date of the competitor.
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the address of the competitor.
        /// </summary>
        [MaxLength(200)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the telephone number of the competitor.
        /// </summary>
        [MaxLength(100)]
        public string TelephoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the height of the competitor.
        /// </summary>
        [Range(160, 220)]
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the weight of the competitor.
        /// </summary>
        [Range(100, 220)]
        public int Weight { get; set; }

        /// <summary>
        /// Gets the competitions in which the competitor competed.
        /// </summary>
        [Hidden]
        public ICollection<Competition> Competitions { get; }

        /// <summary>
        /// Gets the world records which the competitor earned.
        /// </summary>
        [Hidden]
        public ICollection<WorldRecord> WorldRecords { get; }
    }
}
