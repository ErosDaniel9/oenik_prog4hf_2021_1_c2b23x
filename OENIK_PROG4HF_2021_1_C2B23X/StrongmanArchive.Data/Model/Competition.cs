﻿// <copyright file="Competition.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;

[assembly:CLSCompliant(false)]

namespace StrongmanArchive.Data.Model
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    /// <summary>
    /// Entity for the Copmetition table.
    /// </summary>
    [Table("Comptetitions")]
    public class Competition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Competition"/> class.
        /// </summary>
        /// <param name="competitionName">The name of the competition.</param>
        /// <param name="dateOfCompetition">The date of the competition.</param>
        /// <param name="federationID">The primary key of the federation which organizes the competition.</param>
        /// <param name="competitorID">The primary key of the competitor who earned the given placement in the competition.</param>
        /// <param name="placement">The competitor's placement in the competition.</param>
        /// <param name="prize">The competitors earned prize.</param>
        public Competition(string competitionName, DateTime dateOfCompetition, int federationID, int competitorID, int placement, double prize)
        {
            this.CompetitionName = competitionName;
            this.DateOfCompetition = dateOfCompetition;
            this.FederationID = federationID;
            this.CompetitorID = competitorID;
            this.Placement = placement;
            this.Prize = prize;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Competition"/> class.
        /// </summary>
        /// <param name="values">The values of the class in a string list.</param>
        public Competition(Collection<string> values)
        {
            if (values == null)
            {
                string errorMsg = "Üres listát adott meg.";
                throw new ArgumentNullException(errorMsg);
            }

            this.CompetitionName = values[0];
            this.DateOfCompetition = DateTime.Parse(values[1], CultureInfo.InvariantCulture);
            this.FederationID = int.Parse(values[2], CultureInfo.InvariantCulture);
            this.CompetitorID = int.Parse(values[3], CultureInfo.InvariantCulture);
            this.Placement = int.Parse(values[4], CultureInfo.InvariantCulture);
            this.Prize = double.Parse(values[5], CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Competition"/> class.
        /// Empty constructor for DbContext setup.
        /// </summary>
        public Competition()
        {
        }

        /// <summary>
        /// Gets or sets the primary key for the entity.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompetitionID { get; set; }

        /// <summary>
        /// Gets or sets the name of the competition.
        /// </summary>
        [MaxLength(100)]
        public string CompetitionName { get; set; }

        /// <summary>
        /// Gets or sets the date of the competiton.
        /// </summary>
        public DateTime DateOfCompetition { get; set; }

        /// <summary>
        /// Gets or sets the federation id foreign key.
        /// </summary>
        public int? FederationID { get; set; }

        /// <summary>
        /// Gets or sets the federation foreign entity.
        /// </summary>
        [Hidden]
        public Federation Federation { get; set; }

        /// <summary>
        /// Gets or sets the competitor id foreign key.
        /// </summary>
        public int? CompetitorID { get; set; }

        /// <summary>
        /// Gets or sets the competitor foreign entity.
        /// </summary>
        [Hidden]
        public Competitor Competitor { get; set; }

        /// <summary>
        /// Gets or sets the placement parameter. Its value is ranging between 1 and 3.
        /// </summary>
        [Range(1, 3)]
        public int Placement { get; set; }

        /// <summary>
        /// Gets or sets the prize parameter.
        /// </summary>
        public double Prize { get; set; }
    }
}
