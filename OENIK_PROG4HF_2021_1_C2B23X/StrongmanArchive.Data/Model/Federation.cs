﻿// <copyright file="Federation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    /// <summary>
    /// Entity for the Federation table.
    /// </summary>
    [Table("Federations")]
    public class Federation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Federation"/> class.
        /// </summary>
        /// <param name="values">The values of the class in a string list.</param>
        public Federation(Collection<string> values)
        {
            if (values == null)
            {
                string errorMsg = "Üres listát adott meg.";
                throw new ArgumentNullException(errorMsg);
            }

            this.FederationName = values[0];
            this.EstablishmentDate = DateTime.Parse(values[1], CultureInfo.InvariantCulture);
            this.EstablisherName = values[2];
            this.Active = bool.Parse(values[3]);
            this.NetWorth = int.Parse(values[4], CultureInfo.InvariantCulture);
            this.Competitions = new HashSet<Competition>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Federation"/> class.
        /// </summary>
        public Federation()
        {
            this.Competitions = new HashSet<Competition>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Federation"/> class.
        /// </summary>
        /// <param name="federationName">The name of the federation.</param>
        /// <param name="establishmentDate">The date of the federations' establishment.</param>
        /// <param name="establisherName">The name of the federations' establisher.</param>
        /// <param name="active">A boolean value indicating whether the federation is still active.</param>
        /// <param name="netWorth">The net value of the federation.</param>
        public Federation(string federationName, DateTime establishmentDate, string establisherName, bool active, int netWorth = 0)
        {
            this.FederationName = federationName;
            this.EstablishmentDate = establishmentDate;
            this.EstablisherName = establisherName;
            this.Active = active;
            this.NetWorth = netWorth;
            this.Competitions = new HashSet<Competition>();
        }

        /// <summary>
        /// Gets or sets the primary key.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FederationID { get; set; }

        /// <summary>
        /// Gets or sets the name of the federation.
        /// </summary>
        [MaxLength(100)]
        public string FederationName { get; set; }

        /// <summary>
        /// Gets or sets the establishment date of the federation.
        /// </summary>
        public DateTime EstablishmentDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the federations' establisher.
        /// </summary>
        public string EstablisherName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the federation is still active.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the net value of the federation.
        /// </summary>
        public int NetWorth { get; set; }

        /// <summary>
        /// Gets a collection of all the competitions the federation has organized.
        /// </summary>
        [Hidden]
        public ICollection<Competition> Competitions { get; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Federation)
            {
                Federation other = obj as Federation;
                return this.FederationID == other.FederationID &&
                    this.FederationName == other.FederationName &&
                    this.EstablisherName == other.EstablisherName &&
                    this.EstablishmentDate == other.EstablishmentDate &&
                    this.Active == other.Active &&
                    this.NetWorth == other.NetWorth;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
