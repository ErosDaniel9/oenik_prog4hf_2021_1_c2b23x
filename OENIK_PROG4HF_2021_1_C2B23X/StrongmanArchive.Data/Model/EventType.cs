﻿// <copyright file="EventType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Data.Model
{
    /// <summary>
    /// A type indication the juging method for the event.
    /// </summary>
    public enum EventType
    {
        /// <summary>
        /// The event is judged on the distance covered by the competitors.
        /// </summary>
        Distance,

        /// <summary>
        /// The event is judged by the duration the competitor held the object for.
        /// </summary>
        Time,

        /// <summary>
        /// The event is judged by the repetitions the competitor did with a given weight.
        /// </summary>
        Repetitions,

        /// <summary>
        /// The event is judged by the weight the competitor lifted for one repetition.
        /// </summary>
        Weight,
    }
}