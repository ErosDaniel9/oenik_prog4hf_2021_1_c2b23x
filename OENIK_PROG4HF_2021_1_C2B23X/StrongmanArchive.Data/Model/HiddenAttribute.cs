﻿// <copyright file="HiddenAttribute.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Data.Model
{
    using System;

    /// <summary>
    /// An attribute indicating if the given propery should be interpreted by the display layers' methods.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class HiddenAttribute : Attribute
    {
    }
}