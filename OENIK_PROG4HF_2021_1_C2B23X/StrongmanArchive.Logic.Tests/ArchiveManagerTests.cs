﻿// <copyright file="ArchiveManagerTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;

[assembly:CLSCompliant(false)]

namespace StrongmanArchive.Logic.Tests
{
    using Moq;
    using NUnit.Framework;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// Unit tests for the <see cref="Logic.IArchiveManagerLogic"/> methods implemented by the <see cref="Logic.StrongmanArchiveManagerLogic"/> module.
    /// </summary>
    public class ArchiveManagerTests
    {
        /// <summary>
        /// Tests if the <see cref="IArchiveManagerLogic.InsertInto{T}(Repository.IRepository{T}, T)"/> method on the Federation table correctly calls repository methods.
        /// </summary>
        [Test]
        public void TestInsertIntoFederation()
        {
            // Arrange
            Mock<IFederationRepository> mockedRepository = new Mock<IFederationRepository>(MockBehavior.Loose);
            StrongmanArchiveManagerLogic service = new StrongmanArchiveManagerLogic();
            Federation item = new Federation("TestFederation", new DateTime(1999, 02, 02), "test", true);

            // Act
            service.InsertInto(mockedRepository.Object, item);

            // Assert
            mockedRepository.Verify(r => r.Create(item), Times.Once);
            mockedRepository.Verify(r => r.SaveChanges(), Times.Once);
        }

        /// <summary>
        /// Tests if the <see cref="IArchiveManagerLogic.Update{T}(Repository.IRepository{T}, T)"/> method on the Competition table correctly calls repository methods.
        /// </summary>
        [Test]
        public void TestUpdateCompetition()
        {
            // Arramge
            Mock<ICompetitionRepository> mockRepo = new Mock<ICompetitionRepository>(MockBehavior.Loose);
            StrongmanArchiveManagerLogic service = new StrongmanArchiveManagerLogic();
            Competition mock = new Competition();

            // Act
            service.Update(mockRepo.Object, mock);

            // Assert
            mockRepo.Verify(repo => repo.Update(mock), Times.Once);
            mockRepo.Verify(repo => repo.SaveChanges(), Times.Once);
        }

        /// <summary>
        /// Tests if the <see cref="IArchiveManagerLogic.DeleteFrom{T}(Repository.IRepository{T}, T)"/> method on the Competitor table correctly calls repository methods.
        /// </summary>
        [Test]
        public void TestDeleteFromCompetitor()
        {
            // Arrange
            Mock<ICompetitorRepository> mockRepo = new Mock<ICompetitorRepository>(MockBehavior.Loose);
            StrongmanArchiveManagerLogic service = new StrongmanArchiveManagerLogic();
            Competitor mockObj = new Competitor();

            // Act
            service.DeleteFrom(mockRepo.Object, mockObj);

            // Assert
            mockRepo.Verify(repo => repo.Delete(mockObj), Times.Once);
            mockRepo.Verify(repo => repo.SaveChanges(), Times.Once);
        }
    }
}
