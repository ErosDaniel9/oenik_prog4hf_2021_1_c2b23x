﻿// <copyright file="StrongmanSpecificQueriesTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// Tests for the <see cref="Logic.IStrongmanSpecificQueriesLogic"/> methods implemented by the <see cref="Logic.StrongmanArchiveBasicUserLogic"/> module.
    /// </summary>
    public class StrongmanSpecificQueriesTest
    {
        /// <summary>
        /// Tests if the <see cref="IStrongmanSpecificQueriesLogic.GetCompetitorsWithMoreWorldRecordsThan(Repository.IRepository{Competitor}, Repository.IRepository{WorldRecord}, int)"/> method returns correct subset with correct parameters.
        /// </summary>
        [Test]
        public void TestGetCompetitorsWithMoreWorldRecordsThanReturnsCorectSubset()
        {
            // Arrange
            Mock<ICompetitorRepository> mockCompetitorRepository = new Mock<ICompetitorRepository>(MockBehavior.Loose);
            Mock<IWorldRecordRepository> mockWorldRecordRepository = new Mock<IWorldRecordRepository>(MockBehavior.Loose);

            List<Competitor> competitors = new List<Competitor>()
            {
                new Competitor() { CompetitorID = 1, CompetitorName = "Sanyi" },
                new Competitor() { CompetitorID = 2, CompetitorName = "Gizi" },
                new Competitor() { CompetitorID = 3, CompetitorName = "Pityu" },
            };

            List<WorldRecord> worldRecords = new List<WorldRecord>()
            {
                new WorldRecord() { WorldRecordID = 1, CompetitorID = 1 },
                new WorldRecord() { WorldRecordID = 2, CompetitorID = 2 },
                new WorldRecord() { WorldRecordID = 3, CompetitorID = 3 },
                new WorldRecord() { WorldRecordID = 4, CompetitorID = 2 },
                new WorldRecord() { WorldRecordID = 5, CompetitorID = 2 },
                new WorldRecord() { WorldRecordID = 6, CompetitorID = 2 },
                new WorldRecord() { WorldRecordID = 7, CompetitorID = 3 },
                new WorldRecord() { WorldRecordID = 8, CompetitorID = 3 },
                new WorldRecord() { WorldRecordID = 9, CompetitorID = 3 },
                new WorldRecord() { WorldRecordID = 10, CompetitorID = 2 },
            };

            mockCompetitorRepository.Setup(repo => repo.GetAll()).Returns(competitors.AsQueryable());
            mockWorldRecordRepository.Setup(repo => repo.GetAll()).Returns(worldRecords.AsQueryable());

            StrongmanArchiveBasicUserLogic service = new StrongmanArchiveBasicUserLogic();

            List<CompetitorWithRecordNumbers> expectedResult = new List<CompetitorWithRecordNumbers>()
            {
                new CompetitorWithRecordNumbers() { CompetitorName = "Gizi", NumberOfWorldRecords = worldRecords.Where(x => x.CompetitorID == 2).Count() },
                new CompetitorWithRecordNumbers() { CompetitorName = "Pityu", NumberOfWorldRecords = worldRecords.Where(x => x.CompetitorID == 3).Count() },
            };

            // Act
            var result = service.GetCompetitorsWithMoreWorldRecordsThan(mockCompetitorRepository.Object, mockWorldRecordRepository.Object, 2);

            // Assert
            mockCompetitorRepository.Verify(repo => repo.GetAll(), Times.Once);
            mockWorldRecordRepository.Verify(repo => repo.GetAll(), Times.Once);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        /// <summary>
        /// Tests if <see cref="IStrongmanSpecificQueriesLogic.GetCompetitorsWhoEarnedMoreThan(Repository.IRepository{Competitor}, Repository.IRepository{Competition}, double)"/> method returns correct subset with correct parameters.
        /// </summary>
        [Test]
        public void TestGetCompetitorsWhoEarnedMoreThanReturnsCorrectSubset()
        {
            // Arrange
            Mock<ICompetitorRepository> mockCompetitorRepository = new Mock<ICompetitorRepository>(MockBehavior.Loose);
            Mock<ICompetitionRepository> mockCompetitionRepository = new Mock<ICompetitionRepository>(MockBehavior.Loose);

            List<Competitor> competitors = new List<Competitor>()
            {
                new Competitor() { CompetitorID = 1, CompetitorName = "Sanyi" },
                new Competitor() { CompetitorID = 2, CompetitorName = "Gizi" },
                new Competitor() { CompetitorID = 3, CompetitorName = "Pityu" },
            };

            List<Competition> competitions = new List<Competition>()
            {
                new Competition() { CompetitionID = 1, Prize = 10000, CompetitorID = 1 },
                new Competition() { CompetitionID = 2, Prize = 500, CompetitorID = 1 },
                new Competition() { CompetitionID = 3, Prize = 2000, CompetitorID = 1 },
                new Competition() { CompetitionID = 4, Prize = 25743, CompetitorID = 2 },
                new Competition() { CompetitionID = 5, Prize = 2, CompetitorID = 2 },
                new Competition() { CompetitionID = 6, Prize = 504, CompetitorID = 2 },
                new Competition() { CompetitionID = 7, Prize = -59, CompetitorID = 3 },
                new Competition() { CompetitionID = 8, Prize = 0, CompetitorID = 3 },
                new Competition() { CompetitionID = 9, Prize = 21, CompetitorID = 3 },
                new Competition() { CompetitionID = 10, Prize = 5, CompetitorID = 3 },
            };

            List<CompetitorWithEarnedMoney> expectedResult = new List<CompetitorWithEarnedMoney>()
            {
                new CompetitorWithEarnedMoney() { CompetitorName = "Gizi", MoneyEarned = competitions.Where(x => x.CompetitorID == 2).Sum(x => x.Prize) },
                new CompetitorWithEarnedMoney() { CompetitorName = "Sanyi", MoneyEarned = competitions.Where(x => x.CompetitorID == 1).Sum(x => x.Prize) },
            };
            mockCompetitionRepository.Setup(repo => repo.GetAll()).Returns(competitions.AsQueryable());
            mockCompetitorRepository.Setup(repo => repo.GetAll()).Returns(competitors.AsQueryable());

            StrongmanArchiveBasicUserLogic service = new StrongmanArchiveBasicUserLogic();

            // Act
            var result = service.GetCompetitorsWhoEarnedMoreThan(mockCompetitorRepository.Object, mockCompetitionRepository.Object);

            // Assert
            mockCompetitorRepository.Verify(repo => repo.GetAll(), Times.Once);
            mockCompetitionRepository.Verify(repo => repo.GetAll(), Times.Once);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        /// <summary>
        /// Tests if <see cref="IStrongmanSpecificQueriesLogic.GetFederationsWhereAveragePrizeGivenIsBiggerThan(Repository.IRepository{Federation}, Repository.IRepository{Competition}, double)"/> method return correct subset with correct input parameters.
        /// </summary>
        [Test]
        public void TestGetFederationsWhereAveragePrizeGivenIsBiggerThanReturnsCorrectValues()
        {
            // Arrange
            Mock<IFederationRepository> mockFederationRepo = new Mock<IFederationRepository>(MockBehavior.Loose);
            Mock<ICompetitionRepository> mockCompetitionRepo = new Mock<ICompetitionRepository>(MockBehavior.Loose);

            List<Federation> federations = new List<Federation>()
            {
                new Federation() { FederationID = 1, FederationName = "fed1" },
                new Federation() { FederationID = 2, FederationName = "fed2" },
            };

            List<Competition> competitions = new List<Competition>()
            {
                new Competition() { CompetitionID = 1, CompetitionName = "com1", Prize = 5000, FederationID = 1 },
                new Competition() { CompetitionID = 2, CompetitionName = "com2", Prize = 10000, FederationID = 1 },
                new Competition() { CompetitionID = 3, CompetitionName = "com3", Prize = 20000, FederationID = 1 },
                new Competition() { CompetitionID = 4, CompetitionName = "com4", Prize = 1000, FederationID = 2 },
                new Competition() { CompetitionID = 5, CompetitionName = "com5", Prize = 2000, FederationID = 2 },
                new Competition() { CompetitionID = 6, CompetitionName = "com6", Prize = 1500, FederationID = 2 },
            };

            mockFederationRepo.Setup(repo => repo.GetAll()).Returns(federations.AsQueryable());
            mockCompetitionRepo.Setup(repo => repo.GetAll()).Returns(competitions.AsQueryable());

            StrongmanArchiveBasicUserLogic service = new StrongmanArchiveBasicUserLogic();

            List<FederationsWithAveragePrize> expectedResult = new List<FederationsWithAveragePrize>()
            {
                new FederationsWithAveragePrize() { FederationName = "fed1", AveragePrize = competitions.Where(x => x.FederationID == 1).Average(x => x.Prize) },
            };

            // Act
            var result = service.GetFederationsWhereAveragePrizeGivenIsBiggerThan(mockFederationRepo.Object, mockCompetitionRepo.Object);

            // Assert
            mockCompetitionRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockFederationRepo.Verify(repo => repo.GetAll(), Times.Once);

            Assert.That(result, Is.EqualTo(expectedResult));
            Assert.That(result, Is.EquivalentTo(expectedResult));
        }

        /// <summary>
        /// Tests if the <see cref="IStrongmanSpecificQueriesLogic.GetCompetitionsInAGivenYear(Repository.IRepository{Competition}, int)"/> method correctly calls repository methods and that it returns correct subset.
        /// </summary>
        [Test]
        public void TestGetCompetitionsInAGivenYear()
        {
            // Arrange
            Mock<ICompetitionRepository> mockRepo = new Mock<ICompetitionRepository>(MockBehavior.Loose);
            List<Competition> competitions = new List<Competition>()
            {
                new Competition("c1", new DateTime(1999, 02, 02), 1, 3, 3, 10000),
                new Competition("c2", new DateTime(2000, 02, 02), 1, 4, 1, 10000),
                new Competition("c3", new DateTime(1999, 02, 02), 1, 3, 1, 10000),
            };
            List<Competition> expectedResult = new List<Competition>() { competitions[0], competitions[2] };
            mockRepo.Setup(r => r.GetAll()).Returns(competitions.AsQueryable());
            StrongmanArchiveBasicUserLogic service = new StrongmanArchiveBasicUserLogic();

            // Act
            var result = service.GetCompetitionsInAGivenYear(mockRepo.Object, 1999);

            // Assert
            Assert.That(result, Is.EqualTo(expectedResult));
            Assert.That(result, Is.EquivalentTo(expectedResult));
            mockRepo.Verify(rep => rep.GetAll(), Times.Once);
        }
    }
}
