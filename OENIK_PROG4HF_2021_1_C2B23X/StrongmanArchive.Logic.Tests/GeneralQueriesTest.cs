﻿// <copyright file="GeneralQueriesTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// Tests for the <see cref="Logic.IGeneralQueriesLogic"/> methods implemented by the <see cref="Logic.StrongmanArchiveBasicUserLogic"/>module.
    /// </summary>
    public class GeneralQueriesTest
    {
        /// <summary>
        /// Tests if the <see cref="IGeneralQueriesLogic.GetOne{T}(Repository.IRepository{T}, int)"/> method on the WorldRecord table correctly calls repository methods and that it returns the correct value.
        /// </summary>
        /// <param name="id">The identifier of the element.</param>
        [TestCase(3)]
        public void TestGetOneFromWorldRecord(int id)
        {
            // Arrange
            Mock<IWorldRecordRepository> mockRepo = new Mock<IWorldRecordRepository>(MockBehavior.Loose);
            List<WorldRecord> worldRecords = new List<WorldRecord>()
            {
                new WorldRecord() { WorldRecordID = 1, EventName = "e1", DateOfEvent = new DateTime(1999, 02, 02), TypeOfEvent = EventType.Distance, Performance = 16, Weight = 200, CompetitorID = 3 },
                new WorldRecord() { WorldRecordID = 2, EventName = "e2", DateOfEvent = new DateTime(1999, 02, 02), TypeOfEvent = EventType.Distance, Performance = 16, Weight = 200, CompetitorID = 3 },
                new WorldRecord() { WorldRecordID = 3, EventName = "e3", DateOfEvent = new DateTime(1999, 02, 02), TypeOfEvent = EventType.Distance, Performance = 16, Weight = 200, CompetitorID = 3 },
            };
            mockRepo.Setup(repo => repo.GetById(id)).Returns(worldRecords[id - 1]);
            WorldRecord expectedResult = new WorldRecord() { WorldRecordID = 3, EventName = "e3", DateOfEvent = new DateTime(1999, 02, 02), TypeOfEvent = EventType.Distance, Performance = 16, Weight = 200, CompetitorID = 3 };
            StrongmanArchiveBasicUserLogic service = new StrongmanArchiveBasicUserLogic();

            // Act
            var result = service.GetOne(mockRepo.Object, id);

            // Assert
            mockRepo.Verify(repo => repo.GetById(id), Times.Once);
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        /// <summary>
        /// Tests if the <see cref="IGeneralQueriesLogic.GetAll{T}(Repository.IRepository{T})"/> method on the Federation table correctlz calls repository methods and that it returns the correct list.
        /// </summary>
        [Test]
        public void TestGetAllFromFederations()
        {
            // Arrange
            Mock<IFederationRepository> mockRepo = new Mock<IFederationRepository>(MockBehavior.Loose);
            List<Federation> federations = new List<Federation>()
            {
                new Federation() { FederationID = 1, FederationName = "f1", Active = true, EstablisherName = "e1", EstablishmentDate = new DateTime(1800, 11, 07), NetWorth = 10000 },
                new Federation() { FederationID = 2, FederationName = "f2", Active = false, EstablisherName = "e2", EstablishmentDate = new DateTime(1711, 1, 02), NetWorth = 500 },
                new Federation() { FederationID = 3, FederationName = "f3", Active = true, EstablisherName = "e3", EstablishmentDate = new DateTime(2000, 11, 02), NetWorth = -20000 },
            };

            List<Federation> expectedResult = new List<Federation>()
            {
                new Federation() { FederationID = 1, FederationName = "f1", Active = true, EstablisherName = "e1", EstablishmentDate = new DateTime(1800, 11, 07), NetWorth = 10000 },
                new Federation() { FederationID = 2, FederationName = "f2", Active = false, EstablisherName = "e2", EstablishmentDate = new DateTime(1711, 1, 02), NetWorth = 500 },
                new Federation() { FederationID = 3, FederationName = "f3", Active = true, EstablisherName = "e3", EstablishmentDate = new DateTime(2000, 11, 02), NetWorth = -20000 },
            };
            mockRepo.Setup(repo => repo.GetAll()).Returns(federations.AsQueryable());
            StrongmanArchiveBasicUserLogic service = new StrongmanArchiveBasicUserLogic();

            // Act
            var result = service.GetAll(mockRepo.Object);

            // Assert
            mockRepo.Verify(repo => repo.GetAll(), Times.Once);
            Assert.That(result, Is.EqualTo(expectedResult));
            Assert.That(result, Is.EquivalentTo(expectedResult));
        }
    }
}
