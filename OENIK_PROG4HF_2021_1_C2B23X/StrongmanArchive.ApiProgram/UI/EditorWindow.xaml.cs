﻿// <copyright file="EditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.ApiProgram.UI
{
    using System.Windows;
    using StrongmanArchive.ApiProgram.Data;
    using StrongmanArchive.ApiProgram.VM;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class EditorWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        public EditorWindow()
        {
            this.InitializeComponent();

            this.VM = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        /// <param name="oldFederation">The old federation we want to copy from.</param>
        public EditorWindow(Federation oldFederation)
            : this()
        {
            this.VM.Federation = oldFederation;
        }

        /// <summary>
        /// Gets or sets the viewmodel the window uses as data source.
        /// </summary>
        public EditorViewModel VM { get; set; }

        /// <summary>
        /// Gets the federation of the ViewModel.
        /// </summary>
        public Federation Federation { get => this.VM.Federation; }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
