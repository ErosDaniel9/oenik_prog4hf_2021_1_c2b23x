﻿// <copyright file="EstablishmentDateToStringConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.ApiProgram.UI
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Formats output to a datetime display and vica versa.
    /// </summary>
    public class EstablishmentDateToStringConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime date = (DateTime)value;
            return string.Format(culture, $"{date.Year}.{date.Month}.{date.Day}");
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] date = value?.ToString().Split('.');

            int year = int.Parse(date[0], culture);
            int month = int.Parse(date[1], culture);
            int day = int.Parse(date[2], culture);

            return new DateTime(year, month, day);
        }
    }
}
