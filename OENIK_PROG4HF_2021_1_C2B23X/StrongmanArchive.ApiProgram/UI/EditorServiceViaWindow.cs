﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.ApiProgram.UI
{
    using System;
    using StrongmanArchive.ApiProgram.BL;
    using StrongmanArchive.ApiProgram.Data;

    /// <summary>
    /// Implements the IEditorService interface with a ViewModel target.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <inheritdoc/>
        public bool EditFederation(Federation federation)
        {
            EditorWindow window = new EditorWindow(federation);
            return window.ShowDialog() ?? false;
        }
    }
}
