﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;

[assembly:CLSCompliant(false)]

namespace StrongmanArchive.ApiProgram
{
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Messaging;
    using StrongmanArchive.ApiProgram.BL;
    using StrongmanArchive.ApiProgram.UI;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Logic;
    using StrongmanArchive.Repository;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoC.Instance);

            MyIoC.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIoC.Instance.Register<IFederationLogic, FederationLogic>();
            MyIoC.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoC.Instance.Register<IBasicUserLogic, StrongmanArchiveBasicUserLogic>();

            MyIoC.Instance.Register<IArchiveManagerLogic, StrongmanArchiveManagerLogic>();
            MyIoC.Instance.Register<IFederationRepository, FederationRepository>();
            MyIoC.Instance.Register<StrongmanArchiveContext>(() => new StrongmanArchiveContext());
        }
    }
}
