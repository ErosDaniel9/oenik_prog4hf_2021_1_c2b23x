﻿// <copyright file="Federation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.ApiProgram.Data
{
    using System;
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Federation class for data binding.
    /// </summary>
    public class Federation : ObservableObject
    {
        private string name;
        private DateTime establishedIn;
        private string establisher;
        private bool isActive;
        private int netWorth;
        private int id;

        /// <summary>
        /// Initializes a new instance of the <see cref="Federation"/> class.
        /// </summary>
        public Federation()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Federation"/> class.
        /// </summary>
        /// <param name="id">The primary key of the federation.</param>
        /// <param name="name">The name of the federation.</param>
        /// <param name="establishedIn">The date the federation was established in.</param>
        /// <param name="establisher">The name of the federations' establisher.</param>
        /// <param name="isActive">A value indicating whether the federation is still active.</param>
        /// <param name="netWorth">The net worth of the federation.</param>
        public Federation(int id, string name, DateTime establishedIn, string establisher, bool isActive, int netWorth)
        {
            this.id = id;
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.establishedIn = establishedIn;
            this.establisher = establisher ?? throw new ArgumentNullException(nameof(establisher));
            this.isActive = isActive;
            this.netWorth = netWorth;
        }

        /// <summary>
        /// Gets or sets the primary key of the federation.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets name of the federation.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the date the federation was established.
        /// </summary>
        public DateTime EstablishedIn
        {
            get { return this.establishedIn; }
            set { this.Set(ref this.establishedIn, value); }
        }

        /// <summary>
        /// Gets or sets the name of the establisher.
        /// </summary>
        public string Establisher
        {
            get { return this.establisher; }
            set { this.Set(ref this.establisher, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the federation is still active.
        /// </summary>
        public bool IsActive
        {
            get { return this.isActive; }
            set { this.Set(ref this.isActive, value); }
        }

        /// <summary>
        /// Gets or sets the net worth of the federation.
        /// </summary>
        public int NetWorth
        {
            get { return this.netWorth; }
            set { this.Set(ref this.netWorth, value); }
        }

        /// <summary>
        /// Copies all values from another object.
        /// </summary>
        /// <param name="other">The source of the copy.</param>
        public void CopyFrom(Federation other)
        {
            this.GetType().GetProperties().ToList().ForEach(p => p.SetValue(this, p.GetValue(other)));
        }

        /// <summary>
        /// Transforms a Federation Db object into a UI-level federation object.
        /// </summary>
        /// <param name="dbObj">The object we want to transform.</param>
        public void TransformDbElementIntoFederation(StrongmanArchive.Data.Model.Federation dbObj)
        {
            if (dbObj == null)
            {
                string errorMsg = "The given object is empty.";
                throw new ArgumentNullException(errorMsg);
            }

            this.name = dbObj.FederationName;
            this.establishedIn = dbObj.EstablishmentDate;
            this.establisher = dbObj.EstablisherName;
            this.isActive = dbObj.Active;
            this.netWorth = dbObj.NetWorth;
        }
    }
}
