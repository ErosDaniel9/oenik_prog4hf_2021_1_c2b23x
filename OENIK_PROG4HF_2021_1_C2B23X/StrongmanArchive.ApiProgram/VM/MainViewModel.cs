﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.ApiProgram.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using StrongmanArchive.ApiProgram.BL;
    using StrongmanArchive.ApiProgram.Data;

    /// <summary>
    /// A viewmodel for the main window.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IFederationLogic logic;
        private Federation selectedFederation;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic operating on our federations.</param>
        public MainViewModel(IFederationLogic logic)
        {
            this.logic = logic;
            this.Federations = new ObservableCollection<Federation>();

            if (this.IsInDesignMode)
            {
                this.Federations.Add(new Federation(1, "Test fed", new DateTime(1999, 02, 11), "Test establisher", true, 2000));
                this.Federations.Add(new Federation(2, "Test fed2", new DateTime(2008, 02, 11), "Test establisher2", false, 150));
            }
            else
            {
                this.Federations = new ObservableCollection<Federation>();
            }

            this.Insert = new RelayCommand(() => this.logic.Insert(this.Federations));
            this.Update = new RelayCommand(() => this.logic.Update(this.SelectedFederation));
            this.Delete = new RelayCommand(() => this.logic.DeleteFrom(this.Federations, this.SelectedFederation));
            this.Refresh = new RelayCommand(() => this.logic.GetFederations(this.Federations));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IFederationLogic>())
        {
        }

        /// <summary>
        /// Gets the collection containing the federations to display.
        /// </summary>
        public ObservableCollection<Federation> Federations { get; private set; }

        /// <summary>
        /// Gets or sets the selected federation.
        /// </summary>
        public Federation SelectedFederation
        {
            get { return this.selectedFederation; }
            set { this.Set(ref this.selectedFederation, value); }
        }

        /// <summary>
        /// Gets the command redirecting to the logics insert method.
        /// </summary>
        public ICommand Insert { get; private set; }

        /// <summary>
        /// Gets the command redirecting to the logics update method.
        /// </summary>
        public ICommand Update { get; private set; }

        /// <summary>
        /// Gets the command redirecting to the logics delete method.
        /// </summary>
        public ICommand Delete { get; private set; }

        /// <summary>
        /// Gets the command redirecting to the logics refresh method.
        /// </summary>
        public ICommand Refresh { get; private set; }
    }
}
