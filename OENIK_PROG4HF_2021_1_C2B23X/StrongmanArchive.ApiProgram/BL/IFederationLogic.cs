﻿// <copyright file="IFederationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.ApiProgram.BL
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using StrongmanArchive.ApiProgram.Data;

    /// <summary>
    /// All the methods a federation modifier object should inherit.
    /// </summary>
    public interface IFederationLogic
    {
        /// <summary>
        /// Inserts an element into the destination.
        /// </summary>
        /// <param name="destination">Where to insert the new element.</param>
        void Insert(IList<Federation> destination);

        /// <summary>
        /// Updates a singular element.
        /// </summary>
        /// <param name="federationToModify">The element we want to updte.</param>
        void Update(Federation federationToModify);

        /// <summary>
        /// Deletes a singular element from a collection.
        /// </summary>
        /// <param name="federations">The collections we want to delete from.</param>
        /// <param name="federation">The element we want to delete.</param>
        void DeleteFrom(IList<Federation> federations, Federation federation);

        /// <summary>
        /// Queries the db for federations, then fills up the destination with the queried data.
        /// </summary>
        /// <param name="destination">The list we'tt fill up with the queried data.</param>
        void GetFederations(IList<Federation> destination);
    }
}
