﻿// <copyright file="FederationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.ApiProgram.BL
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using Newtonsoft.Json;
    using StrongmanArchive.ApiProgram.Data;
    using StrongmanArchive.Common;
    using StrongmanArchive.Logic;

    /// <summary>
    /// The logic connecting the program layer with the logic layer.
    /// </summary>
    public sealed class FederationLogic : IFederationLogic, IDisposable
    {
        private const string URL = "http://localhost:54477/FederationApi";

        private readonly IEditorService editorService;
        private readonly IMessenger messengerService;
        private readonly IBasicUserLogic dqlLogic;
        private readonly IArchiveManagerLogic dmlLogic;

        private HttpClient httpClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="FederationLogic"/> class.
        /// </summary>
        /// <param name="editorService">The federation editor methods.</param>
        /// <param name="messengerService">The messaging methods.</param>
        /// <param name="dqlLogic">The logic resplonsible for executing the dql queries against the repo.</param>
        /// <param name="dmlLogic">The logic responsible for executing the dml queries againt the repo.</param>
        public FederationLogic(IEditorService editorService, IMessenger messengerService, IBasicUserLogic dqlLogic, IArchiveManagerLogic dmlLogic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.dqlLogic = dqlLogic;
            this.dmlLogic = dmlLogic;
            this.httpClient = new HttpClient();
        }

        /// <inheritdoc/>
        public async void DeleteFrom(IList<Federation> federations, Federation federation)
        {
            if (federations == null || federation == null)
            {
                this.messengerService.Send("Please select a federation!", "ApiResult");
                return;
            }

            var response = await this.httpClient.PostAsJsonAsync($"{URL}/delete", federation).ConfigureAwait(true);
            var result = JsonConvert.DeserializeObject<ApiResult>(await response.Content.ReadAsStringAsync().ConfigureAwait(true));

            if (result.Success)
            {
                federations.Remove(federation);
                this.messengerService.Send("DELETE OK", "ApiResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "ApiResult");
            }
        }

        /// <inheritdoc/>
        public async void GetFederations(IList<Federation> destination)
        {
            destination?.Clear();
            Uri getAllUri = new Uri($"{URL}/all");
            var response = await this.httpClient.GetStringAsync(getAllUri).ConfigureAwait(true);
            JsonConvert.DeserializeObject<List<Federation>>(response)
                .ForEach(fed => destination.Add(fed));
        }

        /// <inheritdoc/>
        public async void Insert(IList<Federation> destination)
        {
            Federation newFederation = new Federation();

            if (this.editorService.EditFederation(newFederation) == true)
            {
                var validationResult = ValidateInputRanges(newFederation);
                if (!validationResult.Success)
                {
                    this.messengerService.Send(validationResult.ErrorMsg, "ApiResult");
                    return;
                }

                var response = await this.httpClient.PostAsJsonAsync($"{URL}/insert", newFederation).ConfigureAwait(true);
                var result = JsonConvert.DeserializeObject<ApiResult>(await response.Content.ReadAsStringAsync().ConfigureAwait(true));
                if (result.Success && destination != null)
                {
                    Federation insertedFederation = await this.GetOneFederation(result.Id).ConfigureAwait(true);
                    destination.Add(insertedFederation);
                    this.messengerService.Send("Successful insert!", "ApiResult");
                }
                else
                {
                    this.messengerService.Send(result.ErrorMsg, "ApiResult");
                }
            }
        }

        /// <inheritdoc/>
        public async void Update(Federation federationToModify)
        {
            if (federationToModify == null)
            {
                this.messengerService.Send("Please select a federation!", "ApiResult");
                return;
            }

            Federation clonedFederation = new Federation();
            clonedFederation.CopyFrom(federationToModify);

            if (this.editorService.EditFederation(clonedFederation) == true)
            {
                var validationResult = ValidateInputRanges(clonedFederation);
                if (!validationResult.Success)
                {
                    this.messengerService.Send(validationResult.ErrorMsg, "ApiResult");
                    return;
                }

                var response = await this.httpClient.PostAsJsonAsync($"{URL}/update", clonedFederation).ConfigureAwait(true);
                var result = JsonConvert.DeserializeObject<ApiResult>(await response.Content.ReadAsStringAsync().ConfigureAwait(true));

                if (result.Success)
                {
                    federationToModify.CopyFrom(await this.GetOneFederation(clonedFederation.Id).ConfigureAwait(true));
                    this.messengerService.Send("Update OK!", "ApiResult");
                }
                else
                {
                    this.messengerService.Send(result.ErrorMsg, "ApiResult");
                }
            }
            else
            {
                this.messengerService.Send("Update Cancelled!", "ApiResult");
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.httpClient.Dispose();
            GC.SuppressFinalize(this);
        }

        private static ApiResult ValidateInputRanges(Federation elementToValidate)
        {
            if (string.IsNullOrEmpty(elementToValidate.Name) ||
                elementToValidate.Name.Length < 5 ||
                elementToValidate.Name.Length > 100)
            {
                return new ApiResult
                {
                    ErrorMsg = "Federation name must be between 5 and 100 characters!",
                    Success = false,
                };
            }
            else if (string.IsNullOrEmpty(elementToValidate.Establisher) ||
                elementToValidate.Establisher.Length < 5 ||
                elementToValidate.Establisher.Length > 100)
            {
                return new ApiResult
                {
                    ErrorMsg = "Establisher must be between 5 and 100 characters!",
                    Success = false,
                };
            }
            else if (elementToValidate.NetWorth < -200000 || elementToValidate.NetWorth > 200000)
            {
                return new ApiResult
                {
                    ErrorMsg = "Net worth must be in the range of [-200000, 200000]!",
                    Success = false,
                };
            }
            else
            {
                return new ApiResult { Success = true };
            }
        }

        private async Task<Federation> GetOneFederation(int id)
        {
            Uri getOneUri = new Uri($"{URL}/one/{id}");
            var response = await this.httpClient.GetStringAsync(getOneUri).ConfigureAwait(true);
            return JsonConvert.DeserializeObject<Federation>(response);
        }
    }
}
