﻿// <copyright file="IStrongmanSpecificQueriesLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository;

    /// <summary>
    /// The queries specific to this implementation of the archive.
    /// </summary>
    public interface IStrongmanSpecificQueriesLogic
    {
        /// <summary>
        /// Gets the federations who gave out more money on average than a user-given double value.
        /// </summary>
        /// <param name="federationRepository">The repository through which we access the federation table of the archive.</param>
        /// <param name="competitionRepository">The repository through which we access the competition table of the archive.</param>
        /// <param name="breakpoint">The user given value we filter by with a default value of 2000.</param>
        /// <returns>A list containing the competitions with the money they earned.</returns>
        public Collection<FederationsWithAveragePrize> GetFederationsWhereAveragePrizeGivenIsBiggerThan(IRepository<Federation> federationRepository, IRepository<Competition> competitionRepository, double breakpoint = 2000);

        /// <summary>
        /// Gets the federations who gave out more money on average than a user-given double value asynchronously.
        /// </summary>
        /// <param name="federationRepository">The repository through which we access the federation table of the archive.</param>
        /// <param name="competitionRepository">The repository through which we access the competition table of the archive.</param>
        /// <param name="breakpoint">The user given value we filter by with a default value of 2000.</param>
        /// <returns>A list containing the competitions with the money they earned.</returns>
        public Task<Collection<FederationsWithAveragePrize>> GetFederationsWhereAveragePrizeGivenIsBiggerThanAsync(IRepository<Federation> federationRepository, IRepository<Competition> competitionRepository, double breakpoint = 2000);

        /// <summary>
        /// Gets the competitors who had more world records than a user-given double value.
        /// </summary>
        /// <param name="competitorRepository">The repository through which we access the competitor table of the archive.</param>
        /// <param name="worldRecordRepository">The repository through which we access the world record table of the archive.</param>
        /// <param name="breakpoint">The user given value we filter by with a default value of 1.</param>
        /// <returns>A list containing the competitions with the money they earned.</returns>
        public Collection<CompetitorWithRecordNumbers> GetCompetitorsWithMoreWorldRecordsThan(IRepository<Competitor> competitorRepository, IRepository<WorldRecord> worldRecordRepository, int breakpoint = 1);

        /// <summary>
        /// Gets the competitors who had more world records than a user-given double value asynchronously.
        /// </summary>
        /// <param name="competitorRepository">The repository through which we access the competitor table of the archive.</param>
        /// <param name="worldRecordRepository">The repository through which we access the world record table of the archive.</param>
        /// <param name="breakpoint">The user given value we filter by with a default value of 1.</param>
        /// <returns>A list containing the competitions with the money they earned.</returns>
        public Task<Collection<CompetitorWithRecordNumbers>> GetCompetitorsWithMoreWorldRecordsThanAsync(IRepository<Competitor> competitorRepository, IRepository<WorldRecord> worldRecordRepository, int breakpoint = 1);

        /// <summary>
        /// Gets the competitors who earned more money than a user-given double value.
        /// </summary>
        /// <param name="competitorRepository">The repository through which we access the competitor table of the archive.</param>
        /// <param name="competitionRepository">The repository through which we access the competition table of the archive.</param>
        /// <param name="breakpoint">The user given value we filter by with a default value of 2000.</param>
        /// <returns>A list containing the competitions with the money they earned.</returns>
        public Collection<CompetitorWithEarnedMoney> GetCompetitorsWhoEarnedMoreThan(IRepository<Competitor> competitorRepository, IRepository<Competition> competitionRepository, double breakpoint = 2000);

        /// <summary>
        /// Gets the competitors who earned more money than a user-given double value asynchronously.
        /// </summary>
        /// <param name="competitorRepository">The repository through which we access the competitor table of the archive.</param>
        /// <param name="competitionRepository">The repository through which we access the competition table of the archive.</param>
        /// <param name="breakpoint">The user given value we filter by with a default value of 2000.</param>
        /// <returns>A list containing the competitions with the money they earned.</returns>
        public Task<Collection<CompetitorWithEarnedMoney>> GetCompetitorsWhoEarnedMoreThanAsync(IRepository<Competitor> competitorRepository, IRepository<Competition> competitionRepository, double breakpoint = 2000);

        /// <summary>
        /// Gets the competitions which were held in the year given by the user.
        /// </summary>
        /// <param name="competitionRepository">The repository through which we access the archive.</param>
        /// <param name="year">The year which we filter by.</param>
        /// <returns>A list containing the competitiions.</returns>
        public Collection<Competition> GetCompetitionsInAGivenYear(IRepository<Competition> competitionRepository, int year);
    }
}