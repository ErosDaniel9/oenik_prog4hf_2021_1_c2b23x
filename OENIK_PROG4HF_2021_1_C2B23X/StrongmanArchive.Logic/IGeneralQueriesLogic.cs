﻿// <copyright file="IGeneralQueriesLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic
{
    using System.Linq;
    using StrongmanArchive.Repository;

    /// <summary>
    /// Methods executing the most basic queries on the archive.
    /// </summary>
    public interface IGeneralQueriesLogic
    {
        /// <summary>
        /// Gets one element from the archive.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="repository">The repository through which we access the archive.</param>
        /// <param name="id">The primary identifier of the element.</param>
        /// <returns>A class containing the selected element.</returns>
        public T GetOne<T>(IRepository<T> repository, int id);

        /// <summary>
        /// Gets a table from the archive.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="repository">The repository through which we access the archive.</param>
        /// <returns>The selected table, which we can execute queries against.</returns>
        public IQueryable<T> GetAll<T>(IRepository<T> repository);
    }
}