﻿// <copyright file="IBasicUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic
{
    /// <summary>
    /// All the methods available for non-admin users visiting the archive.
    /// </summary>
    public interface IBasicUserLogic : IGeneralQueriesLogic, IStrongmanSpecificQueriesLogic
    {
    }
}
