﻿// <copyright file="IArchiveManagerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic
{
    using StrongmanArchive.Repository;

    /// <summary>
    /// Contains all methods used by any archive manager.
    /// </summary>
    public interface IArchiveManagerLogic
    {
        /// <summary>
        /// Inserts a value into the archive.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="repository">The repository through which we access the archive.</param>
        /// <param name="data">The data to be inserted.</param>
        public void InsertInto<T>(IRepository<T> repository, T data);

        /// <summary>
        /// Update a parameter of a single value from the archive.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="repository">The repository through which we access the archive.</param>
        /// <param name="element">The new element.</param>
        public void Update<T>(IRepository<T> repository, T element);

        /// <summary>
        /// Delete a value from the archive.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="repository">The repository through which we access the archive.</param>
        /// <param name="element">The element to delete.</param>
        public void DeleteFrom<T>(IRepository<T> repository, T element);
    }
}
