﻿// <copyright file="StrongmanArchiveBasicUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;

[assembly:CLSCompliant(false)]

namespace StrongmanArchive.Logic
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository;

    /// <summary>
    /// The class representing the non-admin users for the archive.
    /// </summary>
    public class StrongmanArchiveBasicUserLogic : IBasicUserLogic
    {
        /// <inheritdoc/>
        public IQueryable<T> GetAll<T>(IRepository<T> repository)
        {
            return repository?.GetAll();
        }

        /// <inheritdoc/>
        public Collection<Competition> GetCompetitionsInAGivenYear(IRepository<Competition> competitionRepository, int year)
        {
            var query = from competition in competitionRepository?.GetAll()
                        where competition.DateOfCompetition.Year == year
                        select competition;
            return new Collection<Competition>(query.ToList());
        }

        /// <inheritdoc/>
        public Collection<CompetitorWithEarnedMoney> GetCompetitorsWhoEarnedMoreThan(IRepository<Competitor> competitorRepository, IRepository<Competition> competitionRepository, double breakpoint = 2000)
        {
            var query = from competitor in competitorRepository?.GetAll()
                        join competition in competitionRepository?.GetAll() on competitor.CompetitorID equals competition.CompetitorID
                        group new { competitor, competition } by competitor.CompetitorName into grp
                        where grp.Sum(x => x.competition.Prize) >= breakpoint
                        orderby grp.Sum(x => x.competition.Prize) descending
                        select new CompetitorWithEarnedMoney() { CompetitorName = grp.Key, MoneyEarned = grp.Sum(x => x.competition.Prize) };

            return new Collection<CompetitorWithEarnedMoney>(query.ToList());
        }

        /// <inheritdoc/>
        public Task<Collection<CompetitorWithEarnedMoney>> GetCompetitorsWhoEarnedMoreThanAsync(IRepository<Competitor> competitorRepository, IRepository<Competition> competitionRepository, double breakpoint = 2000)
        {
            return Task<Collection<CompetitorWithEarnedMoney>>.Factory.StartNew(() => this.GetCompetitorsWhoEarnedMoreThan(competitorRepository, competitionRepository, breakpoint));
        }

        /// <inheritdoc/>
        public Collection<CompetitorWithRecordNumbers> GetCompetitorsWithMoreWorldRecordsThan(IRepository<Competitor> competitorRepository, IRepository<WorldRecord> worldRecordRepository, int breakpoint = 1)
        {
            var query = from competitor in competitorRepository?.GetAll()
                        join worldRecord in worldRecordRepository?.GetAll() on competitor.CompetitorID equals worldRecord.CompetitorID
                        group new { competitor, worldRecord } by competitor.CompetitorName into grp
                        where grp.Count() >= breakpoint
                        orderby grp.Count() descending
                        select new CompetitorWithRecordNumbers() { CompetitorName = grp.Key, NumberOfWorldRecords = grp.Count() };

            return new Collection<CompetitorWithRecordNumbers>(query.ToList());
        }

        /// <inheritdoc/>
        public Task<Collection<CompetitorWithRecordNumbers>> GetCompetitorsWithMoreWorldRecordsThanAsync(IRepository<Competitor> competitorRepository, IRepository<WorldRecord> worldRecordRepository, int breakpoint = 1)
        {
            return Task<Collection<CompetitorWithRecordNumbers>>.Factory.StartNew(() => this.GetCompetitorsWithMoreWorldRecordsThan(competitorRepository, worldRecordRepository, breakpoint));
        }

        /// <inheritdoc/>
        public Collection<FederationsWithAveragePrize> GetFederationsWhereAveragePrizeGivenIsBiggerThan(IRepository<Federation> federationRepository, IRepository<Competition> competitionRepository, double breakpoint = 2000)
        {
            var query = from federation in federationRepository?.GetAll()
                        join competition in competitionRepository?.GetAll() on federation.FederationID equals competition.FederationID
                        group new { federation, competition } by federation.FederationName into grp
                        where grp.Average(x => x.competition.Prize) >= breakpoint
                        select new FederationsWithAveragePrize() { FederationName = grp.Key, AveragePrize = grp.Average(x => x.competition.Prize) };

            return new Collection<FederationsWithAveragePrize>(query.ToList());
        }

        /// <inheritdoc/>
        public Task<Collection<FederationsWithAveragePrize>> GetFederationsWhereAveragePrizeGivenIsBiggerThanAsync(IRepository<Federation> federationRepository, IRepository<Competition> competitionRepository, double breakpoint = 2000)
        {
            return Task<Collection<FederationsWithAveragePrize>>.Factory.StartNew(() => this.GetFederationsWhereAveragePrizeGivenIsBiggerThan(federationRepository, competitionRepository, breakpoint));
        }

        /// <inheritdoc/>
        public T GetOne<T>(IRepository<T> repository, int id)
        {
            if (repository == null)
            {
                string errorMsg = "A repository adott meg.";
                throw new ArgumentNullException(errorMsg);
            }

            return repository.GetById(id);
        }
    }
}
