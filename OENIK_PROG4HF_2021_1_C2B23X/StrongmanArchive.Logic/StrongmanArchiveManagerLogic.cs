﻿// <copyright file="StrongmanArchiveManagerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic
{
    using StrongmanArchive.Repository;

    /// <summary>
    /// The class representing the admin user for the archive.
    /// </summary>
    public class StrongmanArchiveManagerLogic : IArchiveManagerLogic
    {
        /// <inheritdoc/>
        public void DeleteFrom<T>(IRepository<T> repository, T element)
        {
            repository?.Delete(element);
            repository.SaveChanges();
        }

        /// <inheritdoc/>
        public void InsertInto<T>(IRepository<T> repository, T data)
        {
            repository?.Create(data);
            repository.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update<T>(IRepository<T> repository, T element)
        {
            repository?.Update(element);
            repository.SaveChanges();
        }
    }
}
