﻿// <copyright file="CompetitorWithRecordNumbers.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic
{
    /// <summary>
    /// The helper class for the <see cref="StrongmanArchiveBasicUserLogic.GetCompetitorsWithMoreWorldRecordsThan(Repository.IRepository{Data.Model.Competitor}, Repository.IRepository{Data.Model.WorldRecord}, int)"/> and <see cref="StrongmanArchiveBasicUserLogic.GetCompetitorsWithMoreWorldRecordsThanAsync(Repository.IRepository{Data.Model.Competitor}, Repository.IRepository{Data.Model.WorldRecord}, int)"/> methods.
    /// </summary>
    public class CompetitorWithRecordNumbers
    {
        /// <summary>
        /// Gets or sets the name of the competitor.
        /// </summary>
        public string CompetitorName { get; set; }

        /// <summary>
        /// Gets or sets the number of world records the competitor earned.
        /// </summary>
        public int NumberOfWorldRecords { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is CompetitorWithRecordNumbers)
            {
                CompetitorWithRecordNumbers other = obj as CompetitorWithRecordNumbers;
                return this.CompetitorName == other.CompetitorName &&
                    this.NumberOfWorldRecords == other.NumberOfWorldRecords;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return base.ToString();
        }
    }
}