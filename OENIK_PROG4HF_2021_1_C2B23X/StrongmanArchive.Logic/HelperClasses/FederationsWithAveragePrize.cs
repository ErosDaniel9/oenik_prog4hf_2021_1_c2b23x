﻿// <copyright file="FederationsWithAveragePrize.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic
{
    /// <summary>
    /// Helper class for the <see cref="StrongmanArchiveBasicUserLogic.GetFederationsWhereAveragePrizeGivenIsBiggerThan(Repository.IRepository{Data.Model.Federation}, Repository.IRepository{Data.Model.Competition}, double)"/> and <see cref="StrongmanArchiveBasicUserLogic.GetFederationsWhereAveragePrizeGivenIsBiggerThanAsync(Repository.IRepository{Data.Model.Federation}, Repository.IRepository{Data.Model.Competition}, double)"/> methods.
    /// </summary>
    public class FederationsWithAveragePrize
    {
        /// <summary>
        /// Gets or sets the name of the federation.
        /// </summary>
        public string FederationName { get; set; }

        /// <summary>
        /// Gets or sets the average prize given out by the federation.
        /// </summary>
        public double AveragePrize { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is FederationsWithAveragePrize)
            {
                FederationsWithAveragePrize other = obj as FederationsWithAveragePrize;
                return this.FederationName == other.FederationName &&
                    this.AveragePrize == other.AveragePrize;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return base.ToString();
        }
    }
}