﻿// <copyright file="CompetitorWithEarnedMoney.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Logic
{
    /// <summary>
    /// A helper class for the <see cref="StrongmanArchiveBasicUserLogic.GetCompetitorsWhoEarnedMoreThanAsync(Repository.IRepository{Data.Model.Competitor}, Repository.IRepository{Data.Model.Competition}, double)"/> and <see cref="StrongmanArchiveBasicUserLogic.GetCompetitorsWhoEarnedMoreThanAsync(Repository.IRepository{Data.Model.Competitor}, Repository.IRepository{Data.Model.Competition}, double)"/> methods.
    /// </summary>
    public class CompetitorWithEarnedMoney
    {
        /// <summary>
        /// Gets or sets the name of the competitor.
        /// </summary>
        public string CompetitorName { get; set; }

        /// <summary>
        /// Gets or sets the money earned by the competitor.
        /// </summary>
        public double MoneyEarned { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is CompetitorWithEarnedMoney)
            {
                CompetitorWithEarnedMoney other = obj as CompetitorWithEarnedMoney;
                return this.CompetitorName == other.CompetitorName &&
                    this.MoneyEarned == other.MoneyEarned;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return base.ToString();
        }
    }
}