﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Program.UI
{
    using System;
    using StrongmanArchive.Program.BL;
    using StrongmanArchive.Program.Data;

    /// <summary>
    /// Implements the IEditorService interface with a ViewModel target.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <inheritdoc/>
        public bool EditFederation(Federation federation)
        {
            EditorWindow window = new EditorWindow(federation);
            return window.ShowDialog() ?? false;
        }
    }
}
