﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Program.VM
{
    using System;
    using System.Collections.ObjectModel;
    using GalaSoft.MvvmLight;
    using StrongmanArchive.Program.Data;

    /// <summary>
    /// A viewmodel for the editor service.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private Federation federation;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            if (this.IsInDesignMode)
            {
                // TODO: just add values.
                this.Federation = new Federation("Test fed", new DateTime(1999, 02, 11), "Test establisher", true, 2000);
            }
            else
            {
                this.Federation = new Federation();
            }

            this.PossibleBoolValues = new Collection<string>() { "true", "false" };
        }

        /// <summary>
        /// Gets or sets the dummy object for the VM.
        /// </summary>
        public Federation Federation
        {
            get { return this.federation; }
            set { this.Set(ref this.federation, value); }
        }

        /// <summary>
        /// Gets the possible values of a boolean object.
        /// </summary>
        public Collection<string> PossibleBoolValues { get; private set; }
    }
}
