﻿// <copyright file="MyIoC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Program
{
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// A MyIoc class.
    /// </summary>
    public class MyIoC : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets static constructor.
        /// </summary>
        public static MyIoC Instance { get; private set; } = new MyIoC();
    }
}
