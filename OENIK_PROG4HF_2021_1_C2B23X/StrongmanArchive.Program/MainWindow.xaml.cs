﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Program
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using StrongmanArchive.Program.VM;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
        }

        /// <summary>
        /// Gets or sets the viewModel of the window.
        /// </summary>
        public MainViewModel VM { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.VM = this.FindResource("VM") as MainViewModel;

            Messenger.Default.Register<string>(this, "LogicResult", msg =>
            {
                MessageBox.Show(msg);
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister<string>(this);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            this.VM.Update?.Execute(this.FederationsListBox.SelectedIndex);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            this.VM.Delete?.Execute(this.FederationsListBox.SelectedIndex);
        }
    }
}
