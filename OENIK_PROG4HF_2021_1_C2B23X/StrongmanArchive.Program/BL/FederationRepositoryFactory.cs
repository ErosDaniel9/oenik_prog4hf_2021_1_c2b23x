﻿// <copyright file="FederationRepositoryFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Program.BL
{
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository;

    /// <summary>
    /// A factory giving us a federationRepository.
    /// </summary>
    public class FederationRepositoryFactory
    {
        private StrongmanArchiveContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="FederationRepositoryFactory"/> class.
        /// </summary>
        /// <param name="context">The dbcontext the repository is working on.</param>
        public FederationRepositoryFactory(StrongmanArchiveContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets a new Federationrepository instance.
        /// </summary>
        public FederationRepository FederationRepository => new FederationRepository(this.context);
    }
}
