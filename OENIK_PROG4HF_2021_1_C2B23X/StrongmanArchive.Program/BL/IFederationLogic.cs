﻿// <copyright file="IFederationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Program.BL
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using StrongmanArchive.Program.Data;

    /// <summary>
    /// All the methods a federation modifier object should inherit.
    /// </summary>
    public interface IFederationLogic
    {
        /// <summary>
        /// Inserts an element into the destination.
        /// </summary>
        /// <param name="destination">Where to insert the new element.</param>
        void Insert(IList<Federation> destination);

        /// <summary>
        /// Updates a singular element.
        /// </summary>
        /// <param name="federationToModify">The element we want to updte.</param>
        /// <param name="federationIdx">The index of the federation.</param>
        void Update(Federation federationToModify, int federationIdx);

        /// <summary>
        /// Deletes a singular element from a collection.
        /// </summary>
        /// <param name="federations">The collections we want to delete from.</param>
        /// <param name="federation">The element we want to delete.</param>
        /// <param name="federationIdx">The index of the element in the container.</param>
        void DeleteFrom(IList<Federation> federations, Federation federation, int federationIdx);

        /// <summary>
        /// Returns a collection containing the elements of the Federations table.
        /// </summary>
        /// <returns>A collection containing the elements of the Federations table.</returns>
        IList<Federation> GetFederations();
    }
}
