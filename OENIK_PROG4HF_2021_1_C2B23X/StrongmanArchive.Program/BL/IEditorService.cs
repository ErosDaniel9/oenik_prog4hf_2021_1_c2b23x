﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Program.BL
{
    using StrongmanArchive.Program.Data;

    /// <summary>
    /// The functionality every editor service should have.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Set a federation to the new values.
        /// </summary>
        /// <param name="federation">The federation to be edited.</param>
        /// <returns>A value indication whether the operation was succesful.</returns>
        bool EditFederation(Federation federation);
    }
}
