﻿// <copyright file="FederationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Program.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using StrongmanArchive.Logic;
    using StrongmanArchive.Program.Data;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// The logic connecting the program layer with the logic layer.
    /// </summary>
    public class FederationLogic : IFederationLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private IBasicUserLogic dqlLogic;
        private IArchiveManagerLogic dmlLogic;
        private IFederationRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FederationLogic"/> class.
        /// </summary>
        /// <param name="editorService">The federation editor methods.</param>
        /// <param name="messengerService">The messaging methods.</param>
        /// <param name="dqlLogic">The logic resplonsible for executing the dql queries against the repo.</param>
        /// <param name="dmlLogic">The logic responsible for executing the dml queries againt the repo.</param>
        /// <param name="repository">The repository our logics are working on.</param>
        public FederationLogic(IEditorService editorService, IMessenger messengerService, IBasicUserLogic dqlLogic, IArchiveManagerLogic dmlLogic, IFederationRepository repository)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.dqlLogic = dqlLogic;
            this.dmlLogic = dmlLogic;
            this.repository = repository;
        }

        /// <inheritdoc/>
        public void DeleteFrom(IList<Federation> federations, Federation federation, int federationIdx)
        {
            if (federations == null || federation == null)
            {
                string errorMsg = "Parameter is null.";
                throw new ArgumentNullException(errorMsg);
            }

            if (federations.Remove(federation))
            {
                // Delete
                StrongmanArchive.Data.Model.Federation elementToBeDeleted = new StrongmanArchive.Data.Model.Federation(federation.Name, federation.EstablishedIn, federation.Establisher, federation.IsActive, federation.NetWorth);
                elementToBeDeleted.FederationID = federationIdx + 1;
                this.dmlLogic.DeleteFrom(this.repository, elementToBeDeleted);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<Federation> GetFederations()
        {
            List<Federation> federations = new List<Federation>();
            this.dqlLogic.GetAll(this.repository)
                .ToList()
                .ForEach(fed => federations.Add(ConvertToDisplayObj(fed)));
            return federations;
        }

        /// <inheritdoc/>
        public void Insert(IList<Federation> destination)
        {
            Federation newFederation = new Federation();
            if (destination != null && this.editorService.EditFederation(newFederation) == true)
            {
                destination.Add(newFederation);

                // Insert
                StrongmanArchive.Data.Model.Federation elementToBeInserted = new StrongmanArchive.Data.Model.Federation(newFederation.Name, newFederation.EstablishedIn, newFederation.Establisher, newFederation.IsActive, newFederation.NetWorth);
                this.dmlLogic.InsertInto(this.repository, elementToBeInserted);
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCELLED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void Update(Federation federationToModify, int federationIdx)
        {
            if (federationToModify == null)
            {
                string errorMsg = "Parameter is null.";
                throw new ArgumentNullException(errorMsg);
            }

            Federation clone = new Federation();
            clone.CopyFrom(federationToModify);

            if (this.editorService.EditFederation(clone))
            {
                federationToModify.CopyFrom(clone);

                StrongmanArchive.Data.Model.Federation updateTarget = new StrongmanArchive.Data.Model.Federation(clone.Name, clone.EstablishedIn, clone.Establisher, clone.IsActive, clone.NetWorth);
                updateTarget.FederationID = federationIdx + 1; // 1 indexed
                this.dmlLogic.Update(this.repository, updateTarget);

                this.messengerService.Send("UPDATE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("UPDATE CANCELLED", "LogicResult");
            }
        }

        private static Federation ConvertToDisplayObj(StrongmanArchive.Data.Model.Federation fed)
        {
            return new Federation
            {
                Name = fed.FederationName,
                Establisher = fed.EstablisherName,
                EstablishedIn = fed.EstablishmentDate,
                IsActive = fed.Active,
                NetWorth = fed.NetWorth,
            };
        }
    }
}
