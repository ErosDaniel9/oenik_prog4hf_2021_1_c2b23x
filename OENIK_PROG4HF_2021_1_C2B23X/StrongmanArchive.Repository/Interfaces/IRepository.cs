﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository
{
    using System.Linq;

    /// <summary>
    /// Generic repository interface.
    /// </summary>
    /// <typeparam name="T">The type of the Entity.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Queries the table from a database.
        /// </summary>
        /// <returns>An IQueryable collection containing the table.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Inserts the given element into the Database.
        /// </summary>
        /// <param name="item">The element to be inserted.</param>
        void Create(T item);

        /// <summary>
        /// Deletes an element from the database.
        /// </summary>
        /// <param name="item">The item to delete.</param>
        void Delete(T item);

        /// <summary>
        /// Queries a single element from the database.
        /// </summary>
        /// <param name="id">The identifier of the element to be queried.</param>
        /// <returns>The queried element.</returns>
        T GetById(int id);

        /// <summary>
        /// Updates one parameter from the selected element with the given value.
        /// </summary>
        /// <param name="item">The new item.</param>
        void Update(T item);

        /// <summary>
        /// Saves the changes made in the database.
        /// </summary>
        void SaveChanges();
    }
}
