﻿// <copyright file="ICompetitorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository.Interfaces
{
    using StrongmanArchive.Data.Model;

    /// <summary>
    /// IRepository derivative to be used by Competitor type repositories.
    /// </summary>
    public interface ICompetitorRepository : IRepository<Competitor>
    {
    }
}
