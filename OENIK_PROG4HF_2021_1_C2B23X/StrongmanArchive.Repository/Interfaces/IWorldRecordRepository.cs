﻿// <copyright file="IWorldRecordRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository.Interfaces
{
    using StrongmanArchive.Data.Model;

    /// <summary>
    /// IRepository derivative to be used by WorldRecord type repositories.
    /// </summary>
    public interface IWorldRecordRepository : IRepository<WorldRecord>
    {
    }
}
