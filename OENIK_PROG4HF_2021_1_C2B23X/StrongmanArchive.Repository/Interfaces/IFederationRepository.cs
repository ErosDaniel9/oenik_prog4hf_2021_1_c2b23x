﻿// <copyright file="IFederationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository.Interfaces
{
    using StrongmanArchive.Data.Model;

    /// <summary>
    /// IRepository derivative to be used by Federation type repositories.
    /// </summary>
    public interface IFederationRepository : IRepository<Federation>
    {
    }
}
