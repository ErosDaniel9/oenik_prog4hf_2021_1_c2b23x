﻿// <copyright file="ICompetitionRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository.Interfaces
{
    using StrongmanArchive.Data.Model;

    /// <summary>
    /// IRepository derivative to be used by Competition type repositories.
    /// </summary>
    public interface ICompetitionRepository : IRepository<Competition>
    {
    }
}
