﻿// <copyright file="WorldRecordRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository.Implementations;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// Repository class working on the WorldRecord entity.
    /// </summary>
    public class WorldRecordRepository : BaseRepository, IWorldRecordRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorldRecordRepository"/> class.
        /// </summary>
        /// <param name="context">The database to work on.</param>
        public WorldRecordRepository(StrongmanArchiveContext context)
        {
            this.context = context;
        }

        /// <inheritdoc/>
        public void Create(WorldRecord item)
        {
            this.context.Add(item);
        }

        /// <inheritdoc/>
        public void Delete(WorldRecord item)
        {
            this.context.Remove(item);
        }

        /// <inheritdoc/>
        public IQueryable<WorldRecord> GetAll()
        {
            return this.context.WorldRecords;
        }

        /// <inheritdoc/>
        public WorldRecord GetById(int id)
        {
            return this.context.WorldRecords.Find(id);
        }

        /// <inheritdoc/>
        public void SaveChanges()
        {
            this.context.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(WorldRecord item)
        {
            WorldRecord worldRecord = this.context.WorldRecords.Where(
              x => x.WorldRecordID == item.WorldRecordID).SingleOrDefault();
            if (worldRecord != null)
            {
                this.context.Entry(worldRecord).CurrentValues.SetValues(item);
            }
        }
    }
}
