﻿// <copyright file="CompetitorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository.Implementations;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// Repository class working on the Competitor entity.
    /// </summary>
    public class CompetitorRepository : BaseRepository, ICompetitorRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompetitorRepository"/> class.
        /// </summary>
        /// <param name="context">The database to work on.</param>
        public CompetitorRepository(StrongmanArchiveContext context)
        {
            this.context = context;
        }

        /// <inheritdoc/>
        public void Create(Competitor item)
        {
            this.context.Add(item);
        }

        /// <inheritdoc/>
        public void Delete(Competitor item)
        {
            this.context.Remove(item);
        }

        /// <inheritdoc/>
        public IQueryable<Competitor> GetAll()
        {
            return this.context.Competitors;
        }

        /// <inheritdoc/>
        public Competitor GetById(int id)
        {
            return this.context.Competitors.Find(id);
        }

        /// <inheritdoc/>
        public void SaveChanges()
        {
            this.context.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(Competitor item)
        {
            Competitor competitor = this.context.Competitors.Where(
              x => x.CompetitorID == item.CompetitorID).SingleOrDefault();
            if (competitor != null)
            {
                this.context.Entry(competitor).CurrentValues.SetValues(item);
            }
        }
    }
}
