﻿// <copyright file="BaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository.Implementations
{
    using StrongmanArchive.Data.Model;

    /// <summary>
    /// An abstract repository containing the DbContext object to operate on.
    /// </summary>
    public class BaseRepository
    {
        /// <summary>
        /// The DbContext the repository operates on.
        /// </summary>
        protected StrongmanArchiveContext context;
    }
}
