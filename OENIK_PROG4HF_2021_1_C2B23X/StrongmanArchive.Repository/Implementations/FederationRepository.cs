﻿// <copyright file="FederationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StrongmanArchive.Repository
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository.Implementations;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// Repository class working on the Federation entity.
    /// </summary>
    public class FederationRepository : BaseRepository, IFederationRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FederationRepository"/> class.
        /// </summary>
        /// <param name="context">The database to work on.</param>
        public FederationRepository(StrongmanArchiveContext context)
        {
            this.context = context;
        }

        /// <inheritdoc/>
        public void Create(Federation item)
        {
            this.context.Add(item);
        }

        /// <inheritdoc/>
        public void Delete(Federation item)
        {
            Federation federation = this.context.Federations.Where(
              x => x.FederationID == item.FederationID).SingleOrDefault();
            if (federation != null)
            {
                this.context.Remove(federation);
            }
        }

        /// <inheritdoc/>
        public IQueryable<Federation> GetAll()
        {
            return this.context.Federations;
        }

        /// <inheritdoc/>
        public Federation GetById(int id)
        {
            return this.context.Federations.Find(id);
        }

        /// <inheritdoc/>
        public void SaveChanges()
        {
            this.context.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(Federation item)
        {
            Federation federation = this.context.Federations.Where(
              x => x.FederationID == item.FederationID).SingleOrDefault();
            if (federation != null)
            {
                this.context.Entry(federation).CurrentValues.SetValues(item);
            }
        }
    }
}
