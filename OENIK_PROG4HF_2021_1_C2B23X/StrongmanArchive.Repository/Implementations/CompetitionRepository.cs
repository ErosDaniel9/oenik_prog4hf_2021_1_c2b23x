﻿// <copyright file="CompetitionRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;

[assembly:CLSCompliant(false)]

namespace StrongmanArchive.Repository
{
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using StrongmanArchive.Data.Model;
    using StrongmanArchive.Repository.Implementations;
    using StrongmanArchive.Repository.Interfaces;

    /// <summary>
    /// Repository class working on the Competition entity.
    /// </summary>
    public class CompetitionRepository : BaseRepository, ICompetitionRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompetitionRepository"/> class.
        /// </summary>
        /// <param name="context">The database to work on.</param>
        public CompetitionRepository(StrongmanArchiveContext context)
        {
            this.context = context;
        }

        /// <inheritdoc/>
        public void Create(Competition item)
        {
            this.context.Add(item);
        }

        /// <inheritdoc/>
        public void Delete(Competition item)
        {
            this.context.Competitions.Remove(item);
        }

        /// <inheritdoc/>
        public IQueryable<Competition> GetAll()
        {
            return this.context.Competitions;
        }

        /// <inheritdoc/>
        public Competition GetById(int id)
        {
            return this.context.Competitions.Find(id);
        }

        /// <inheritdoc/>
        public void SaveChanges()
        {
            this.context.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(Competition item)
        {
            Competition competition = this.context.Competitions.Where(
              x => x.CompetitionID == item.CompetitionID).SingleOrDefault();
            if (competition != null)
            {
                this.context.Entry(competition).CurrentValues.SetValues(item);
            }
        }
    }
}
