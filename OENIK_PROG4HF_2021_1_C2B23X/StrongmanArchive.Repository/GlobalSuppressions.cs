﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "protected datafield is needed.", Scope = "member", Target = "~F:StrongmanArchive.Repository.Implementations.BaseRepository.context")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "protected datafield is needed.", Scope = "member", Target = "~F:StrongmanArchive.Repository.Implementations.BaseRepository.context")]
